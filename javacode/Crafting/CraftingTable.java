package jespo655.cc.Crafting;

import jespo655.cc.Crafting.Items.Equipment;
import jespo655.cc.Crafting.Wrappers.CraftingMaterial;
import jespo655.cc.Crafting.Wrappers.Item;
import jespo655.cc.Crafting.Wrappers.ItemStack;
import jespo655.cc.Crafting.Items.Weapon;
import jespo655.cc.Crafting.Materials.BaseMaterial;
import jespo655.cc.Crafting.Materials.DecorationMaterial;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * All crafting is done through the crafting table.
 * This class contains all crafting mechanics.
 */
public class CraftingTable {



    private static final Random RANDOM = new Random();










    // Tries to craft the specified equipment. Returns null if failed to craft.
    public static Equipment craftEquipment(Equipment.Slot slot, Equipment.EquipmentType equipmentType, List<CraftingMaterial> materials, int craftingSkill) {

        RecipeContainer.Recipe recipe = RecipeContainer.getEquipmentRecipe(slot, equipmentType);
        AvailableMaterials availableMaterials = new AvailableMaterials(materials);

        return availableMaterials.craftEquipment(recipe, true, craftingSkill);

    }


    public static Equipment craftWeapon(Weapon.Type weaponType, List<CraftingMaterial> materials, int craftingSkill) {

        RecipeContainer.Recipe recipe = RecipeContainer.getWeaponRecipe(weaponType);
        AvailableMaterials availableMaterials = new AvailableMaterials(materials);

        return availableMaterials.craftEquipment(recipe, true, craftingSkill);

    }





    private static class AvailableMaterials {

        private List<ItemStack> leather = new ArrayList<>();
        private List<ItemStack> metal = new ArrayList<>();
        private List<ItemStack> cloth = new ArrayList<>();
        private List<ItemStack> wood = new ArrayList<>();
        private List<ItemStack> decorationMaterials = new ArrayList<>();
        private List<CraftingMaterial> otherMaterials = new ArrayList<>();

        private float totalLeather = 0;
        private float totalMetal = 0;
        private float totalCloth = 0;
        private float totalWood = 0;
        private int totalDecorationMaterial = 0;

        AvailableMaterials() {}
        AvailableMaterials(List<CraftingMaterial> materials) {
            addMaterials(materials);
        }

        public void addMaterials(List<CraftingMaterial> materials) {
            for (CraftingMaterial mat : materials) {
                if (!mat.isUsed()) {
                    if (mat instanceof ItemStack) {
                        ItemStack is = (ItemStack) mat;
                        if (is.item instanceof BaseMaterial) {
                            float quantity = is.getQuantity().floatValue();
                            switch (((BaseMaterial) is.item).type) {
                                case LEATHER:
                                    if (is.addToList(leather))
                                        totalLeather += quantity;
                                    break;
                                case METAL:
                                    if (is.addToList(metal))
                                        totalMetal += quantity;
                                    break;
                                case CLOTH:
                                    if (is.addToList(cloth))
                                        totalCloth += quantity;
                                    break;
                                case WOOD:
                                    if (is.addToList(wood))
                                        totalWood += quantity;
                                    break;
                            }
                        } else if (is.item instanceof DecorationMaterial) {
                            int quantity = is.getQuantity().intValue();
                            if (is.addToList(decorationMaterials))
                                totalDecorationMaterial += quantity;
                        } else
                            System.out.println("CraftingTable: Found stackable crafting material of unknown type: " + is.item.getClass());
                    } else
                          otherMaterials.add(mat);
                }
            }
        }


        public boolean canCraftItem(RecipeContainer.Recipe recipe) {
            // can make it in the first place?
            if (!recipe.craftable) return false;

            // have enough base materials?
            if (totalLeather < recipe.leather || totalMetal < recipe.metal || totalCloth < recipe.cloth || totalWood < recipe.wood) return false;

            // have all other required materials?
            // TODO: known bug: can incorrectly find the same item twice
            //      this method will return the wrong result if required materials contains two of the same item and only one is available
            for (Item.Identifier id : recipe.otherRequiredMaterials) {
                boolean reqMatIsAvailable = false;
                for (CraftingMaterial cm : otherMaterials) {
                    if (cm.getIdentifier() == id) {
                        reqMatIsAvailable = true;
                        break;
                    }
                }
                if (!reqMatIsAvailable)
                    return false;
            }
            return true;
        }


        public Equipment craftEquipment(RecipeContainer.Recipe recipe, boolean useMaterials, int craftingSkill) {

            if (!canCraftItem(recipe))
                return Equipment.FAILED_CRAFT;


            // go through all materials and summarize min and max rolls for all stats
            // after summary is done, randomize each stat in that range

            Equipment maxStatEq = new Equipment(recipe.craftedItemIdentifier);
            Equipment minStatEq = new Equipment(recipe.craftedItemIdentifier);

            Equipment eq;
            if (recipe.craftedItemIdentifier.isWeapon())
                eq = new Weapon(recipe.craftedItemIdentifier);
            else
                eq = new Equipment(recipe.craftedItemIdentifier);

            // base materials
            for (ItemStack is : leather) {
                float quantityToUse = is.getQuantity().floatValue() / totalLeather * recipe.leather;
                BaseMaterial bm = (BaseMaterial)is.item;
                addStats(bm, quantityToUse, recipe.leather, minStatEq, maxStatEq, craftingSkill - bm.level);
                if (useMaterials)
                    is.split(is.getQuantity().floatValue() / totalLeather * recipe.leather).addToList(eq.usedMaterials);
                else
                    is.clone(is.getQuantity().floatValue() / totalLeather * recipe.leather).addToList(eq.usedMaterials);
            }
            for (ItemStack is : metal) {
                float quantityToUse = is.getQuantity().floatValue() / totalMetal * recipe.metal;
                BaseMaterial bm = (BaseMaterial)is.item;
                addStats(bm, quantityToUse, recipe.metal, minStatEq, maxStatEq, craftingSkill - bm.level);
                if (useMaterials)
                    is.split(is.getQuantity().floatValue() / totalMetal * recipe.metal).addToList(eq.usedMaterials);
                else
                    is.clone(is.getQuantity().floatValue() / totalMetal * recipe.metal).addToList(eq.usedMaterials);
            }
            for (ItemStack is : cloth) {
                float quantityToUse = is.getQuantity().floatValue() / totalCloth * recipe.cloth;
                BaseMaterial bm = (BaseMaterial)is.item;
                addStats(bm, quantityToUse, recipe.cloth, minStatEq, maxStatEq, craftingSkill - bm.level);
                if (useMaterials)
                    is.split(is.getQuantity().floatValue() / totalCloth * recipe.cloth).addToList(eq.usedMaterials);
                else
                    is.clone(is.getQuantity().floatValue() / totalCloth * recipe.cloth).addToList(eq.usedMaterials);
            }
            for (ItemStack is : wood) {
                float quantityToUse = is.getQuantity().floatValue() / totalWood * recipe.wood;
                BaseMaterial bm = (BaseMaterial)is.item;
                addStats(bm, quantityToUse, recipe.wood, minStatEq, maxStatEq, craftingSkill - bm.level);
                if (useMaterials)
                    is.split(is.getQuantity().floatValue() / totalWood * recipe.wood).addToList(eq.usedMaterials);
                else
                    is.clone(is.getQuantity().floatValue() / totalWood * recipe.wood).addToList(eq.usedMaterials);
            }

            // choose decorations in order
            int decorationsLeftToUse = recipe.maxDecorations;
            for (ItemStack is : decorationMaterials) {
                DecorationMaterial dm = (DecorationMaterial)is.item;
                int decorationsToUse = Math.max(is.getQuantity().intValue(), decorationsLeftToUse);

                addStats(dm, decorationsToUse, minStatEq, maxStatEq, craftingSkill - dm.level);
                if (useMaterials)
                    is.use(decorationsToUse);
                decorationsLeftToUse -= decorationsToUse;
                if (decorationsLeftToUse <= 0)
                    break;
            }

            // use required materials. These add weight but very limited stats otherwise.
            for (Item.Identifier id : recipe.otherRequiredMaterials) {
                for (CraftingMaterial cm : otherMaterials) {
                    if (cm.getIdentifier() == id) {
                        addStats(cm, minStatEq, maxStatEq, craftingSkill - cm.getLevel(), recipe.otherMaterialsStatCoefficient);
                        if (useMaterials)
                            cm.use();
                    }
                }
            }

            /*  // RANDOM selection of decorations (?)
            int usedDecorations = 0;
            while (decorationMaterials.size() > recipe.maxDecorations - usedDecorations) {
                int i = RANDOM.nextInt(decorationMaterials.size());

            }
            */

            // create the final item

            // eq.x = coefficient * (min.x + (max.x - min.x) * randomFloat)

            eq.weight = minStatEq.weight + (maxStatEq.weight - minStatEq.weight) * RANDOM.nextFloat();
            eq.armour =  recipe.armourCoefficient * (minStatEq.armour + (maxStatEq.armour - minStatEq.armour) * RANDOM.nextFloat());
            float flexibility = Math.min(recipe.maxFlexibility, recipe.flexCoefficient * (minStatEq.flexibility + (maxStatEq.flexibility - minStatEq.flexibility) * RANDOM.nextFloat()));
            eq.flexibility = recipe.minFlexibility + (recipe.maxFlexibility - recipe.minFlexibility) * flexibility;

            eq.secondaryStats.healthRegeneration = recipe.strCoefficient * (minStatEq.secondaryStats.healthRegeneration + (maxStatEq.secondaryStats.healthRegeneration - minStatEq.secondaryStats.healthRegeneration) * RANDOM.nextFloat());
            eq.secondaryStats.movementSpeed = recipe.dexCoefficient * (minStatEq.secondaryStats.movementSpeed + (maxStatEq.secondaryStats.movementSpeed - minStatEq.secondaryStats.movementSpeed) * RANDOM.nextFloat());
            eq.secondaryStats.elementalResistance = recipe.intCoefficient * (minStatEq.secondaryStats.elementalResistance + (maxStatEq.secondaryStats.elementalResistance - minStatEq.secondaryStats.elementalResistance) * RANDOM.nextFloat());
            eq.secondaryStats.manaRegeneration = recipe.intCoefficient * (minStatEq.secondaryStats.manaRegeneration + (maxStatEq.secondaryStats.manaRegeneration - minStatEq.secondaryStats.manaRegeneration) * RANDOM.nextFloat());

            eq.secondaryStats.strength = recipe.strCoefficient * (minStatEq.secondaryStats.strength + (maxStatEq.secondaryStats.strength - minStatEq.secondaryStats.strength) * RANDOM.nextFloat());
            eq.secondaryStats.dexterity = recipe.dexCoefficient * (minStatEq.secondaryStats.dexterity + (maxStatEq.secondaryStats.dexterity - minStatEq.secondaryStats.dexterity) * RANDOM.nextFloat());
            eq.secondaryStats.intelligence = recipe.intCoefficient * (minStatEq.secondaryStats.intelligence + (maxStatEq.secondaryStats.intelligence - minStatEq.secondaryStats.intelligence) * RANDOM.nextFloat());

            if (eq instanceof Weapon) {
                Weapon w = (Weapon) eq;
                w.damage = recipe.wdc.calculateDamage(w.weaponType,w.weight,w.flexibility,w.armour);
                w.attackSpeed = recipe.wsc.calculateAttackSpeed(w.weaponType,w.weight,w.flexibility,w.armour);
            }



            /*
            System.out.println("During crafting: ");
            minStatEq.name = "MIN";
            maxStatEq.name = "MAX";

            minStatEq.print();
            System.out.println();
            maxStatEq.print();
            System.out.println();
            System.out.printf("flexC: %.1f, minEqF: %.1f, maxEqF: %.1f, minRecF: %.1f, maxRecF: %.1f, tempF: %.1f, finalF: %.1f\n\n",
                    recipe.flexCoefficient,minStatEq.flexibility,maxStatEq.flexibility,recipe.minFlexibility,recipe.maxFlexibility,flexibility,eq.flexibility);
                    */

            return eq;
        }
    }





    // addStats summarizes all stats into min and max equipments.
    // NOTE: recipe stat coefficients are not accounted for here.
    private static void addStats(BaseMaterial mat, float quantity, float totalQuantity, Equipment min, Equipment max, int levelDifference) {
        if (quantity == 0) return;

        min.weight += mat.weight * quantity;
        min.flexibility += mat.flexibility * quantity / totalQuantity;

        float minC = minPotentialUsage(levelDifference) * quantity;
        min.armour += mat.toughness * minC;
        min.secondaryStats.healthRegeneration += mat.secondaryStats.healthRegeneration * minC;
        min.secondaryStats.movementSpeed += mat.secondaryStats.movementSpeed * minC;
        min.secondaryStats.elementalResistance += mat.secondaryStats.elementalResistance * minC;
        min.secondaryStats.manaRegeneration += mat.secondaryStats.manaRegeneration * minC;
        min.secondaryStats.strength += mat.secondaryStats.strength * minC;
        min.secondaryStats.dexterity += mat.secondaryStats.dexterity * minC;
        min.secondaryStats.intelligence += mat.secondaryStats.intelligence * minC;


        max.weight += mat.weight * quantity;
        max.flexibility += mat.flexibility * quantity / totalQuantity;

        float maxC = maxPotentialUsage(levelDifference) * quantity;
        max.armour += mat.toughness * maxC;
        max.secondaryStats.healthRegeneration += mat.secondaryStats.healthRegeneration * maxC;
        max.secondaryStats.movementSpeed += mat.secondaryStats.movementSpeed * maxC;
        max.secondaryStats.elementalResistance += mat.secondaryStats.elementalResistance * maxC;
        max.secondaryStats.manaRegeneration += mat.secondaryStats.manaRegeneration * maxC;
        max.secondaryStats.strength += mat.secondaryStats.strength * maxC;
        max.secondaryStats.dexterity += mat.secondaryStats.dexterity * maxC;
        max.secondaryStats.intelligence += mat.secondaryStats.intelligence * maxC;

    }

    // addStats summarizes all stats into min and max equipments.
    // NOTE: recipe stat coefficients are not accounted for here.
    private static void addStats(DecorationMaterial mat, int quantity, Equipment min, Equipment max, int levelDifference) {
        if (quantity == 0) return;

        min.weight += mat.weight * quantity;

        float minC = minPotentialUsage(levelDifference) * quantity;
        min.secondaryStats.healthRegeneration += mat.secondaryStats.healthRegeneration * minC;
        min.secondaryStats.movementSpeed += mat.secondaryStats.movementSpeed * minC;
        min.secondaryStats.elementalResistance += mat.secondaryStats.elementalResistance * minC;
        min.secondaryStats.manaRegeneration += mat.secondaryStats.manaRegeneration * minC;
        min.secondaryStats.strength += mat.secondaryStats.strength * minC;
        min.secondaryStats.dexterity += mat.secondaryStats.dexterity * minC;
        min.secondaryStats.intelligence += mat.secondaryStats.intelligence * minC;


        max.weight += mat.weight * quantity;

        float maxC = maxPotentialUsage(levelDifference) * quantity;
        max.secondaryStats.healthRegeneration += mat.secondaryStats.healthRegeneration * maxC;
        max.secondaryStats.movementSpeed += mat.secondaryStats.movementSpeed * maxC;
        max.secondaryStats.elementalResistance += mat.secondaryStats.elementalResistance * maxC;
        max.secondaryStats.manaRegeneration += mat.secondaryStats.manaRegeneration * maxC;
        max.secondaryStats.strength += mat.secondaryStats.strength * maxC;
        max.secondaryStats.dexterity += mat.secondaryStats.dexterity * maxC;
        max.secondaryStats.intelligence += mat.secondaryStats.intelligence * maxC;

    }


    // addStats summarizes all stats into min and max equipments.
    // NOTE: recipe stat coefficients are not accounted for here.
    private static void addStats(CraftingMaterial mat, Equipment min, Equipment max, int levelDifference, float statCoefficient) {

        min.weight += mat.getWeight();

        float minC = minPotentialUsage(levelDifference) * statCoefficient;
        min.secondaryStats.healthRegeneration += mat.getSecondaryStats().healthRegeneration * minC;
        min.secondaryStats.movementSpeed += mat.getSecondaryStats().movementSpeed * minC;
        min.secondaryStats.elementalResistance += mat.getSecondaryStats().elementalResistance * minC;
        min.secondaryStats.manaRegeneration += mat.getSecondaryStats().manaRegeneration * minC;
        min.secondaryStats.strength += mat.getSecondaryStats().strength * minC;
        min.secondaryStats.dexterity += mat.getSecondaryStats().dexterity * minC;
        min.secondaryStats.intelligence += mat.getSecondaryStats().intelligence * minC;


        max.weight += mat.getWeight();

        float maxC = maxPotentialUsage(levelDifference) * statCoefficient;
        max.secondaryStats.healthRegeneration += mat.getSecondaryStats().healthRegeneration * maxC;
        max.secondaryStats.movementSpeed += mat.getSecondaryStats().movementSpeed * maxC;
        max.secondaryStats.elementalResistance += mat.getSecondaryStats().elementalResistance * maxC;
        max.secondaryStats.manaRegeneration += mat.getSecondaryStats().manaRegeneration * maxC;
        max.secondaryStats.strength += mat.getSecondaryStats().strength * maxC;
        max.secondaryStats.dexterity += mat.getSecondaryStats().dexterity * maxC;
        max.secondaryStats.intelligence += mat.getSecondaryStats().intelligence * maxC;

    }


    private static float maxPotentialUsage(int levelDifference) {
        if (levelDifference < - 100)
            return 0;
        else if (levelDifference < - 20)
            return 0.25f + levelDifference/100 * 0.25f;  // at level -20, max = 0.2f
        else if (levelDifference < 0)
            return 1 + levelDifference/20 * 0.8f;
        else
            return 1;
    }

    private static float minPotentialUsage(int levelDifference) {
        if (levelDifference < -10)
            return 0;
        else if (levelDifference < 0)
            return 0.8f + levelDifference / 10 * 0.8f; // at level -10, min = 0
        else if (levelDifference < 100)
            return 0.8f + levelDifference / 100 * 0.2f;
        else
            return 1;
    }

    /*
    private static float averagePotentialUsage(int levelDifference) {
        float min = minPotentialUsage(levelDifference);
        float max = maxPotentialUsage(levelDifference);
        return (min + max) / 2;
    }

    private static float randomPotentialUsage(int levelDifference, float randomFloat) {
        float min = minPotentialUsage(levelDifference);
        float max = maxPotentialUsage(levelDifference);
        return min + (max - min) * randomFloat;
    }
    */



}

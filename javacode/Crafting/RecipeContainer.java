package jespo655.cc.Crafting;

import jespo655.cc.Crafting.Items.Equipment;
import jespo655.cc.Crafting.Wrappers.Item;
import jespo655.cc.Crafting.Items.Weapon;
import jespo655.cc.Crafting.Materials.BaseMaterial;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class contains information about all recipes that can be crafted in the game.
 */
public class RecipeContainer {













    public static final Recipe NONCRAFTABLE_ITEM;

    public static final Recipe LEATHER_BOOTS;
    public static final Recipe LEATHER_BELT;
    public static final Recipe LEATHER_GLOVES;
    public static final Recipe LEATHER_CHEST;
    public static final Recipe LEATHER_LEGS;
    public static final Recipe LEATHER_HAT;
    public static final Recipe LEATHER_SHIELD;

    public static final Recipe PLATE_BOOTS;
    public static final Recipe PLATE_GLOVES;
    public static final Recipe PLATE_CHEST;
    public static final Recipe PLATE_LEGS;
    public static final Recipe PLATE_HELMET;
    public static final Recipe PLATE_SMALL_SHIELD;
    public static final Recipe PLATE_LARGE_SHIELD;

    public static final Recipe WOODEN_SHIELD;

    public static final Recipe CHAINMAIL_BOOTS;
    public static final Recipe CHAINMAIL_GLOVES;
    public static final Recipe CHAINMAIL_CHEST;
    public static final Recipe CHAINMAIL_LEGS;
    public static final Recipe CHAINMAIL_HELMET;

    public static final Recipe CLOTH_BOOTS;
    public static final Recipe CLOTH_GLOVES;
    public static final Recipe CLOTH_CHEST;
    public static final Recipe CLOTH_LEGS;
    public static final Recipe CLOTH_HAT;
    public static final Recipe CLOTH_ROBE;

    public static final Recipe RING;
    public static final Recipe AMULET;

    public static final Recipe DAGGER;
    public static final Recipe SWORD_1H;
    public static final Recipe SWORD_2H;
    public static final Recipe BATTLE_AXE_1H;
    public static final Recipe BATTLE_AXE_2H;
    public static final Recipe ASSAULT_SPEAR_1H;
    public static final Recipe ASSAULT_SPEAR_2H;
    public static final Recipe POLEARM;
    public static final Recipe WARHAMMER_1H;
    public static final Recipe WARHAMMER_2H;
    public static final Recipe FIST_WEAPON;
    public static final Recipe BOW;
    public static final Recipe STAFF;



    public static Recipe getEquipmentRecipe(Equipment.Slot slot, Equipment.EquipmentType equipmentType) {
        switch (slot) {
            case FEET:
                switch (equipmentType) {
                    case LEATHER: return LEATHER_BOOTS;
                    case PLATE: return PLATE_BOOTS;
                    case CHAINMAIL: return CHAINMAIL_BOOTS;
                    case CLOTH: return CLOTH_BOOTS;
                }
                break;
            case BELT:
                if (equipmentType == Equipment.EquipmentType.LEATHER)
                    return LEATHER_BELT;
                break;
            case HANDS:
                switch (equipmentType) {
                    case LEATHER: return LEATHER_GLOVES;
                    case PLATE: return PLATE_GLOVES;
                    case CHAINMAIL: return CHAINMAIL_GLOVES;
                    case CLOTH: return CLOTH_GLOVES;
                }
                break;
            case CHEST:
                switch (equipmentType) {
                    case LEATHER: return LEATHER_CHEST;
                    case PLATE: return PLATE_CHEST;
                    case CHAINMAIL: return CHAINMAIL_CHEST;
                    case CLOTH: return CLOTH_CHEST;
                }
                break;
            case HEAD:
                switch (equipmentType) {
                    case LEATHER: return LEATHER_HAT;
                    case PLATE: return PLATE_HELMET;
                    case CHAINMAIL: return CHAINMAIL_HELMET;
                    case CLOTH: return CLOTH_HAT;
                }
                break;
            case LEGS:
                switch (equipmentType) {
                    case LEATHER: return LEATHER_LEGS;
                    case PLATE: return PLATE_LEGS;
                    case CHAINMAIL: return CHAINMAIL_LEGS;
                    case CLOTH: return CLOTH_LEGS;
                }
                break;
            case SMALL_SHIELD:
                switch (equipmentType) {
                    case LEATHER: return LEATHER_SHIELD;
                    case PLATE: return PLATE_SMALL_SHIELD;
                    case OTHER: return WOODEN_SHIELD;
                }
                break;
            case LARGE_SHIELD:
                if (equipmentType == Equipment.EquipmentType.PLATE)
                    return PLATE_LARGE_SHIELD;
                break;
            case AMULET:
                return AMULET;
            case RING:
                return RING;
        }
        return NONCRAFTABLE_ITEM;
    }



    public static Recipe getWeaponRecipe(Weapon.Type weaponType) {
        switch (weaponType) {
            case DAGGER:
                return DAGGER;

            case SWORD_1H:
                return SWORD_1H;

            case SWORD_2H:
                return SWORD_2H;

            case BATTLE_AXE_1H:
                return BATTLE_AXE_1H;

            case BATTLE_AXE_2H:
                return BATTLE_AXE_2H;

            case ASSAULT_SPEAR_1H:
                return ASSAULT_SPEAR_1H;

            case ASSAULT_SPEAR_2H:
                return ASSAULT_SPEAR_2H;

            case POLEARM:
                return POLEARM;

            case WARHAMMER_1H:
                return WARHAMMER_1H;

            case WARHAMMER_2H:
                return WARHAMMER_2H;

            case FIST_WEAPON:
                return FIST_WEAPON;

            case BOW:
                return BOW;

            case STAFF:
                return STAFF;

            default:
                return NONCRAFTABLE_ITEM;
        }
    }











































    static {

        NONCRAFTABLE_ITEM = new Recipe(false);

        // specify what materials is needed to craft

        final RecipeTemplate headTemplate = new RecipeTemplate(15, 2);
        final RecipeTemplate chestTemplate = new RecipeTemplate(30, 2);
        final RecipeTemplate gloveTemplate = new RecipeTemplate(10, 1);
        final RecipeTemplate bootTemplate = new RecipeTemplate(10, 1);
        final RecipeTemplate legsTemplate = new RecipeTemplate(25, 1);
        final RecipeTemplate smallShieldTemplate = new RecipeTemplate(15, 1);
        final RecipeTemplate bigShieldTemplate = new RecipeTemplate(30, 2);
        final RecipeTemplate beltTemplate = new RecipeTemplate(10, 1);
        final RecipeTemplate robeTemplate = new RecipeTemplate(40, 3);
        final RecipeTemplate jewelleryTemplate = new RecipeTemplate(5, 1);

        // setCoefficients(float armourC, float minFlex, float maxFlex, float flexC, float strC, float dexC, float intC)
        //                                  armC  minFl maxFl flC  strC  dexC  intC
        headTemplate.setCoefficients(       0.7f, 0.8f,  1,    1,   1,    1,   1.2f);
        chestTemplate.setCoefficients(       1,   0.2f, 0.9f,  1,   1,    1,    1);
        gloveTemplate.setCoefficients(      0.5f, 0.8f,  1,    1,  1.2f,  1,    1);
        bootTemplate.setCoefficients(       0.5f, 0.9f,  1,    1,   1,   1.2f,  1);
        legsTemplate.setCoefficients(       0.9f, 0.2f, 0.9f,  1,   1,    1,    1);
        smallShieldTemplate.setCoefficients( 1,   0.3f, 0.9f,  1,   1,    1,    1);
        bigShieldTemplate.setCoefficients(  0.7f, 0.2f, 0.7f,  1,   1,    1,    1);
        beltTemplate.setCoefficients(       0.2f, 0.9f,  1,    1,  1.5f, 1.5f, 1.5f);
        robeTemplate.setCoefficients(       0.9f, 0.2f, 0.9f,  1,   1,    1,    1);
        jewelleryTemplate.setCoefficients(   0,   0.8f,  1,    1,   2,    2,    2);

        final float MATERIAL_COEFFICIENT_PLATE = 0.8f;
        final float MATERIAL_COEFFICIENT_CHAINMAIL = 0.7f;
        final float MATERIAL_COEFFICIENT_LEATHER = 1f;
        final float MATERIAL_COEFFICIENT_CLOTH = 1f;


        LEATHER_BOOTS = new Recipe(Item.Identifier.LEATHER_BOOTS, bootTemplate, BaseMaterial.Type.LEATHER, MATERIAL_COEFFICIENT_LEATHER);
        LEATHER_BELT = new Recipe(Item.Identifier.LEATHER_BELT, beltTemplate, BaseMaterial.Type.LEATHER, MATERIAL_COEFFICIENT_LEATHER);
        LEATHER_GLOVES = new Recipe(Item.Identifier.LEATHER_GLOVES, gloveTemplate, BaseMaterial.Type.LEATHER, MATERIAL_COEFFICIENT_LEATHER);
        LEATHER_CHEST = new Recipe(Item.Identifier.LEATHER_CHEST, chestTemplate, BaseMaterial.Type.LEATHER, MATERIAL_COEFFICIENT_LEATHER);
        LEATHER_LEGS = new Recipe(Item.Identifier.LEATHER_LEGS, legsTemplate, BaseMaterial.Type.LEATHER, MATERIAL_COEFFICIENT_LEATHER);
        LEATHER_HAT = new Recipe(Item.Identifier.LEATHER_HELMET, headTemplate, BaseMaterial.Type.LEATHER, MATERIAL_COEFFICIENT_LEATHER);
        LEATHER_SHIELD = new Recipe(Item.Identifier.LEATHER_SMALL_SHIELD, smallShieldTemplate, BaseMaterial.Type.LEATHER, MATERIAL_COEFFICIENT_LEATHER);

        PLATE_BOOTS = new Recipe(Item.Identifier.PLATE_BOOTS, bootTemplate, BaseMaterial.Type.METAL, MATERIAL_COEFFICIENT_PLATE, Item.Identifier.LEATHER_BOOTS);
        PLATE_GLOVES = new Recipe(Item.Identifier.PLATE_GLOVES, gloveTemplate, BaseMaterial.Type.METAL, MATERIAL_COEFFICIENT_PLATE, Item.Identifier.LEATHER_GLOVES);
        PLATE_CHEST = new Recipe(Item.Identifier.PLATE_CHEST, chestTemplate, BaseMaterial.Type.METAL, MATERIAL_COEFFICIENT_PLATE, Item.Identifier.LEATHER_CHEST);
        PLATE_LEGS = new Recipe(Item.Identifier.PLATE_LEGS, legsTemplate, BaseMaterial.Type.METAL, MATERIAL_COEFFICIENT_PLATE, Item.Identifier.LEATHER_LEGS);
        PLATE_HELMET = new Recipe(Item.Identifier.PLATE_HELMET, headTemplate, BaseMaterial.Type.METAL, MATERIAL_COEFFICIENT_PLATE, Item.Identifier.LEATHER_HELMET);
        PLATE_SMALL_SHIELD = new Recipe(Item.Identifier.PLATE_SMALL_SHIELD, smallShieldTemplate, BaseMaterial.Type.METAL, MATERIAL_COEFFICIENT_PLATE, Item.Identifier.LEATHER_SMALL_SHIELD);
        PLATE_LARGE_SHIELD = new Recipe(Item.Identifier.PLATE_LARGE_SHIELD, bigShieldTemplate, BaseMaterial.Type.METAL, MATERIAL_COEFFICIENT_PLATE, Item.Identifier.LEATHER_LARGE_SHIELD);

        WOODEN_SHIELD = new Recipe(Item.Identifier.WOODEN_SMALL_SHIELD, smallShieldTemplate, BaseMaterial.Type.WOOD, 1f);

        CHAINMAIL_BOOTS = new Recipe(Item.Identifier.CHAINMAIL_BOOTS, bootTemplate, BaseMaterial.Type.METAL, MATERIAL_COEFFICIENT_CHAINMAIL, Item.Identifier.LEATHER_BOOTS);
        CHAINMAIL_GLOVES = new Recipe(Item.Identifier.CHAINMAIL_GLOVES, gloveTemplate, BaseMaterial.Type.METAL, MATERIAL_COEFFICIENT_CHAINMAIL, Item.Identifier.LEATHER_GLOVES);
        CHAINMAIL_CHEST = new Recipe(Item.Identifier.CHAINMAIL_CHEST, chestTemplate, BaseMaterial.Type.METAL, MATERIAL_COEFFICIENT_CHAINMAIL, Item.Identifier.LEATHER_CHEST);
        CHAINMAIL_LEGS = new Recipe(Item.Identifier.CHAINMAIL_LEGS, legsTemplate, BaseMaterial.Type.METAL, MATERIAL_COEFFICIENT_CHAINMAIL, Item.Identifier.LEATHER_LEGS);
        CHAINMAIL_HELMET = new Recipe(Item.Identifier.CHAINMAIL_HELMET, headTemplate, BaseMaterial.Type.METAL, MATERIAL_COEFFICIENT_CHAINMAIL, Item.Identifier.LEATHER_HELMET);

        CLOTH_BOOTS = new Recipe(Item.Identifier.CLOTH_BOOTS, bootTemplate, BaseMaterial.Type.CLOTH, MATERIAL_COEFFICIENT_CLOTH);
        CLOTH_GLOVES = new Recipe(Item.Identifier.CLOTH_GLOVES, gloveTemplate, BaseMaterial.Type.CLOTH, MATERIAL_COEFFICIENT_CLOTH);
        CLOTH_CHEST = new Recipe(Item.Identifier.CLOTH_CHEST, chestTemplate, BaseMaterial.Type.CLOTH, MATERIAL_COEFFICIENT_CLOTH);
        CLOTH_LEGS = new Recipe(Item.Identifier.CLOTH_LEGS, legsTemplate, BaseMaterial.Type.CLOTH, MATERIAL_COEFFICIENT_CLOTH);
        CLOTH_HAT = new Recipe(Item.Identifier.CLOTH_HELMET, headTemplate, BaseMaterial.Type.CLOTH, MATERIAL_COEFFICIENT_CLOTH);
        CLOTH_ROBE = new Recipe(Item.Identifier.CLOTH_FULL_BODY, robeTemplate, BaseMaterial.Type.CLOTH, MATERIAL_COEFFICIENT_CLOTH);

        RING = new Recipe(Item.Identifier.RING, jewelleryTemplate, BaseMaterial.Type.METAL, 1f);
        AMULET = new Recipe(Item.Identifier.AMULET, jewelleryTemplate, BaseMaterial.Type.METAL, 1f);

        // Recipe (float leather, float metal, float cloth, float wood, int maxDecorations, Object ... otherRequiredMaterials) {
        DAGGER = new Recipe(Item.Identifier.DAGGER, 0, 7, 0, 3, 1);
        SWORD_1H = new Recipe(Item.Identifier.SWORD_1H, 0, 12, 0, 3, 1);
        SWORD_2H = new Recipe(Item.Identifier.SWORD_2H, 0, 18, 0, 5, 2);
        BATTLE_AXE_1H = new Recipe(Item.Identifier.BATTLE_AXE_1H, 0, 10, 0, 5, 1);
        BATTLE_AXE_2H = new Recipe(Item.Identifier.BATTLE_AXE_2H, 0, 15, 0, 8, 2);
        ASSAULT_SPEAR_1H = new Recipe(Item.Identifier.ASSAULT_SPEAR_1H, 0, 3, 0, 10, 1);
        ASSAULT_SPEAR_2H = new Recipe(Item.Identifier.ASSAULT_SPEAR_2H, 0, 3, 0, 15, 2);
        POLEARM = new Recipe(Item.Identifier.POLEARM, 0, 8, 0, 15, 2);
        WARHAMMER_1H = new Recipe(Item.Identifier.WARHAMMER_1H, 0, 12, 0, 5, 2);
        WARHAMMER_2H = new Recipe(Item.Identifier.WARHAMMER_2H, 0, 18, 0, 8, 3);
        FIST_WEAPON = new Recipe(Item.Identifier.FIST_WEAPON, 5, 2, 0, 0, 1);
        BOW = new Recipe(Item.Identifier.BOW, 0, 0, 0, 20, 2);
        STAFF = new Recipe(Item.Identifier.STAFF, 0, 0, 0, 25, 2);


        // setCoefficients(float armourC, float minFlex, float maxFlex, float flexC, float strC, float dexC, float intC)
        //                                armC  minFl maxFl flC  strC  dexC  intC
        DAGGER.setCoefficients(            1,    0,    1,    1,  0.8f,  1,    1);
        SWORD_1H.setCoefficients(          1,    0,    1,    1,   1,    1,    1);
        SWORD_2H.setCoefficients(          1,    0,    1,    1,   1,    1,    1);
        BATTLE_AXE_1H.setCoefficients(     1,    0,    1,    1,   1,    1,   0.8f);
        BATTLE_AXE_2H.setCoefficients(     1,    0,    1,    1,   1,    1,   0.8f);
        ASSAULT_SPEAR_1H.setCoefficients(  1,    0,    1,    1,   1,    1,   0.8f);
        ASSAULT_SPEAR_2H.setCoefficients(  1,    0,    1,    1,   1,    1,   0.8f);
        POLEARM.setCoefficients(           1,    0,    1,    1,  1.2f, 0.8f, 0.8f);
        WARHAMMER_1H.setCoefficients(      1,    0,    1,    1,  1.2f, 0.8f,  1);
        WARHAMMER_2H.setCoefficients(      1,    0,    1,    1,  1.2f, 0.8f,  1);
        FIST_WEAPON.setCoefficients(       1,    0,    1,    1,   1,   1.2f,  1);
        BOW.setCoefficients(               1,    0,    1,    1,  0.5f, 1.5f, 0.5f);
        STAFF.setCoefficients(             1,    0,    1,    1,   1,   0.5f,  2);



        // specify how the item will turn out when created


        // setCoefficients(float armourC, float minFlex, float maxFlex, float strC, float dexC, float intC) {

        CHAINMAIL_BOOTS.armourCoefficient *= 0.8;
        CHAINMAIL_GLOVES.armourCoefficient *= 0.8;
        CHAINMAIL_CHEST.armourCoefficient *= 0.8;
        CHAINMAIL_LEGS.armourCoefficient *= 0.8;
        CHAINMAIL_HELMET.armourCoefficient *= 0.8;

        CHAINMAIL_BOOTS.flexCoefficient *= 1.2;
        CHAINMAIL_GLOVES.flexCoefficient *= 1.2;
        CHAINMAIL_CHEST.flexCoefficient *= 1.2;
        CHAINMAIL_LEGS.flexCoefficient *= 1.2;
        CHAINMAIL_HELMET.flexCoefficient *= 1.2;

        /*
        LEATHER_BOOTS;
        LEATHER_BELT;
        LEATHER_GLOVES;
        LEATHER_CHEST;
        LEATHER_LEGS;
        LEATHER_HAT;
        LEATHER_SHIELD;
        PLATE_BOOTS;
        PLATE_GLOVES;
        PLATE_CHEST;
        PLATE_LEGS;
        PLATE_HELMET;
        PLATE_SMALL_SHIELD;
        PLATE_LARGE_SHIELD;
        WOODEN_SHIELD;
        CLOTH_BOOTS;
        CLOTH_GLOVES;
        CLOTH_CHEST;
        CLOTH_LEGS;
        CLOTH_HAT;
        CLOTH_ROBE;
        RING;
        AMULET;
        DAGGER;
        SWORD_1H;
        SWORD_2H;
        BATTLE_AXE_1H;
        BATTLE_AXE_2H;
        ASSAULT_SPEAR_1H;
        ASSAULT_SPEAR_2H;
        POLEARM;
        WARHAMMER_1H;
        WARHAMMER_2H;
        */
        FIST_WEAPON.wdc = (type, w, f, t) -> (float)Math.sqrt((w+1) * (t+1));
        /*
        BOW;
        STAFF;
        */















    }


    public static class Recipe {

        // material requirements
        public boolean craftable = true;
        public Item.Identifier craftedItemIdentifier;
        public float leather;
        public float metal;
        public float cloth;
        public float wood;
        public int maxDecorations;
        public List<Item.Identifier> otherRequiredMaterials;
        public float otherMaterialsStatCoefficient = 0.2f;


        // stat  coefficients
        // (weight is always the sum of the weight of the materials used)
        public float armourCoefficient = 1;     // [0, 1] this notes how much of the material actually give protection
        public float minFlexibility = 0;
        public float maxFlexibility = 1;       // [0, 1] this notes how flexible the design is, regardless of material
        public float flexCoefficient = 1;

        // 0 flexibility on materials -> 0.2 flexibility on item (minFlexibility)
        // 1 flexibility on materials -> 0.8 flexibility on item (maxFlexibility)
        // (the average of all materials * flexCoefficient) give final flexibility

        // secondary stat coefficients
        public float strCoefficient = 1;
        public float dexCoefficient = 1;
        public float intCoefficient = 1;


        // on weapons:
        // armour, weight -> damage (different formula for different weapons)
        // weight -> attack speed (heavy -> slow)

        public interface WeaponDamageCalculator {
            // calculate the damage per attack with the weapon
            // this should be in the range 0-1000 (damage numbers are still being decided on)
            float calculateDamage(Weapon.Type weaponType, float weight, float flexibility, float toughness);
        }

        public interface WeaponSpeedCalculator {
            // calculate the number of attacks / second with the weapon
            // this should be in the range 0.5 - 2
            float calculateAttackSpeed(Weapon.Type weaponType, float weight, float flexibility, float toughness);
        }

        // default functions:
        public WeaponDamageCalculator wdc = (type, w, f, t) -> {
            // heavier and tougher -> higher damage
            if (type.isRanged())
                return (float) Math.sqrt(w * t); // placeholder for better ranged weapon formula
            else if (type.isTwoHanded())
                return (float) Math.sqrt(w * t) * 1.2f;
            else
                return (float) Math.sqrt(w * t);
        };


        public WeaponSpeedCalculator wsc = (type, w, f, t) -> {
            // heavier -> slower, interval [0.5, 2]
            if (type.isRanged())
                return 0.5f+1.5f*(float)Math.pow(2,-w/(5*Math.max(0.01,f)));        // high flex (close to 1) -> high attack speed
                                                                                    // low flex (close to 0) -> low attack speed
            else if (type.isTwoHanded())
                return 0.5f+1.5f*(float)Math.pow(2,-w/4);
            else
                return 0.5f+1.5f*(float)Math.pow(2,-w/4);
        };








        public void setCoefficients(float armourC, float minFlex, float maxFlex, float flexC, float strC, float dexC, float intC) {
            armourCoefficient = armourC;
            minFlexibility = minFlex;
            maxFlexibility = maxFlex;
            flexCoefficient = flexC;
            strCoefficient = strC;
            dexCoefficient = dexC;
            intCoefficient = intC;
        }




        public Recipe () {
            otherRequiredMaterials = new ArrayList<>();
        }

        public Recipe (boolean craftable) {
            if (!craftable) {
                this.craftable = false;
                leather = Float.MAX_VALUE;
                metal = Float.MAX_VALUE;
                cloth = Float.MAX_VALUE;
                wood = Float.MAX_VALUE;
            }
            otherRequiredMaterials = new ArrayList<>();
        }

        public Recipe (Item.Identifier craftedItemIdentifier, float leather, float metal, float cloth, float wood, int maxDecorations, Item.Identifier... otherRequiredMaterials) {
            this.craftedItemIdentifier = craftedItemIdentifier;
            this.leather = leather;
            this.metal = metal;
            this.cloth = cloth;
            this.wood = wood;
            this.maxDecorations = maxDecorations;
            if (otherRequiredMaterials == null || otherRequiredMaterials.length == 0)
                this.otherRequiredMaterials = new ArrayList<>();
            else
                this.otherRequiredMaterials = Arrays.asList(otherRequiredMaterials);
        }


        public Recipe(Item.Identifier craftedItemIdentifier, RecipeTemplate template, BaseMaterial.Type baseMaterial, float baseCoefficient, Item.Identifier... otherRequiredMaterials) {
            this.craftedItemIdentifier = craftedItemIdentifier;
            switch (baseMaterial) {
                case LEATHER:
                    leather = template.baseMaterial * baseCoefficient;
                    break;
                case METAL:
                    metal = template.baseMaterial * baseCoefficient;
                    break;
                case CLOTH:
                    cloth = template.baseMaterial * baseCoefficient;
                    break;
                case WOOD:
                    wood = template.baseMaterial * baseCoefficient;
                    break;
            }
            maxDecorations = template.maxDecorations;
            if (otherRequiredMaterials == null || otherRequiredMaterials.length == 0)
                this.otherRequiredMaterials = new ArrayList<>();
            else
                this.otherRequiredMaterials = Arrays.asList(otherRequiredMaterials);
            this.otherRequiredMaterials.addAll(template.otherRequiredMaterials);

            armourCoefficient = template.armourCoefficient;
            minFlexibility = template.minFlexibility;
            maxFlexibility = template.maxFlexibility;
            flexCoefficient = template.flexCoefficient;
            strCoefficient = template.strCoefficient;
            dexCoefficient = template.dexCoefficient;
            intCoefficient = template.intCoefficient;
        }
    }


    private static class RecipeTemplate extends Recipe {

        public float baseMaterial;
        public int maxDecorations;

        public RecipeTemplate() {}

        public RecipeTemplate(float baseMaterial, int maxDecorations, Item.Identifier... otherRequiredMaterials) {
            this.baseMaterial = baseMaterial;
            this.maxDecorations = maxDecorations;
            if (otherRequiredMaterials == null || otherRequiredMaterials.length == 0)
                this.otherRequiredMaterials = new ArrayList<>();
            else
                this.otherRequiredMaterials = Arrays.asList(otherRequiredMaterials);
        }

    }



    private static RecipeContainer ourInstance = new RecipeContainer();

    public static RecipeContainer getInstance() {
        return ourInstance;
    }

    private RecipeContainer() {
    }
}

package jespo655.cc.Crafting.Items;

import jespo655.cc.Crafting.Wrappers.Item;
import jespo655.cc.Crafting.Wrappers.StackableItem;

/**
 * Gold can be dropped by enemies and exchanged for goods.
 */
public class Gold implements StackableItem<Integer> {

    public Gold() {}

    @Override
    public Identifier getIdentifier() {
        return Identifier.GOLD;
    }

    @Override
    public Integer getMaxStackSize() {
        return Integer.MAX_VALUE;
    }

    @Override
    public Class<Integer> getQuantityClass() {
        return Integer.class;
    }

}

package jespo655.cc.Crafting.Items;

/**
 *
 */
public class Weapon extends Equipment {

    public static enum Type {
        DAGGER,
        SWORD_1H, SWORD_2H,
        BATTLE_AXE_1H, BATTLE_AXE_2H,
        ASSAULT_SPEAR_1H, ASSAULT_SPEAR_2H,
        POLEARM,
        WARHAMMER_1H, WARHAMMER_2H,
        FIST_WEAPON,
        BOW,
        STAFF,
        NONE;

        public boolean isTwoHanded() {
            switch (this) {
                case SWORD_2H:
                case BATTLE_AXE_2H:
                case ASSAULT_SPEAR_2H:
                case POLEARM:
                case WARHAMMER_2H:
                case BOW:
                case STAFF:
                    return true;
            }
            return false;
        }

        public boolean isRanged() {
            return this == BOW;
        }
    }

    public Weapon(Identifier identifier) {
        super(identifier);
        this.weaponType = identifier.getWeaponType();
    }

    // primary weapon specific stats
    public final Type weaponType;
    public float damage;            // the damage per hit
    public float attackSpeed;       // attacks / second
    //public float reach;           // how far you can reach with a melee attack
    //public float range;           // how far you can throw or shoot with the weapon


    @Override
    public void print() {
        if (this == FAILED_CRAFT) System.out.println("FAILED_CRAFT");
        else {
            System.out.printf("Level %d %s with name \"%s\"\n", level, weaponType, name);
            System.out.printf("Req: level %d, %d str, %d dex, %d int\n", levelRequirement, strengthRequirement, dexterityRequirement, intelligenceRequirement);
            System.out.printf("Weight: %.1f, Damage: %.1f, Attack speed: %.2f, (%.2f DPS)\n", weight, damage, attackSpeed, damage * attackSpeed);
            secondaryStats.print();
        }
    }
}

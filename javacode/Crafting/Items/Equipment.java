package jespo655.cc.Crafting.Items;

import jespo655.cc.Crafting.Wrappers.CraftingMaterial;
import jespo655.cc.Crafting.Wrappers.Item;
import jespo655.cc.Crafting.Wrappers.SecondaryStats;

import java.util.ArrayList;
import java.util.List;

/**
 *  Equipment is all kinds of armour and jewellery that can be used by a character.
 */
public class Equipment implements Item, CraftingMaterial {


    public static final Equipment FAILED_CRAFT;

    static {
        FAILED_CRAFT = new Equipment(Identifier.UNKNOWN);
        FAILED_CRAFT.name = "Failed craft";
    }



    /** ---------------------------------------------------------------------------------------------------------
     *
     *            Stats
     *
     * ----------------------------------------------------------------------------------------------------------
     */

    private Identifier identifier;

    public Equipment(Identifier identifier) {
        this.identifier = identifier;
        name = identifier.toString();
        slot = identifier.getSlot();
        equipmentType = identifier.getEquipmentType();
        secondaryStats = new SecondaryStats();
    }


    // requirements
    public int levelRequirement;
    public int strengthRequirement;
    public int dexterityRequirement;
    public int intelligenceRequirement;

    public boolean canBeEquipped(int level, int strength, int dexterity, int intelligence) {
        return level >= levelRequirement && strength >= strengthRequirement && dexterity >= dexterityRequirement && intelligence >= intelligenceRequirement;
    }



    public String name = "";
    public final Slot slot;
    public final EquipmentType equipmentType;
    public int level;

    // primary stats
    public float weight;
    public float flexibility; // [0, 1]
    public float armour;

    // secondary stats
    public final SecondaryStats secondaryStats;

    // A list of all materials used to craft this item
    public final List<CraftingMaterial> usedMaterials = new ArrayList<>();



    /** ---------------------------------------------------------------------------------------------------------
     *
     *            Usage as crafting material
     * Some equipment items can be used to make other items
     *
     * ----------------------------------------------------------------------------------------------------------
     */

    private boolean isUsed = false;

    @Override
    public CraftingMaterial use() {
        isUsed = true;
        return this;
    }

    @Override
    public boolean isUsed() {
        return isUsed;
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public float getWeight() {
        return weight;
    }

    @Override
    public SecondaryStats getSecondaryStats() {
        return secondaryStats;
    }






    @Override
    public Identifier getIdentifier() {
        return identifier;
    }









    // public int durability; //ska det finnas?







    public void print() {
        if (this == FAILED_CRAFT) System.out.println("FAILED_CRAFT");
        else {
            System.out.printf("Level %d %s %s with name \"%s\"\n", level, equipmentType, slot, name);
            System.out.printf("Req: level %d, %d str, %d dex, %d int\n", levelRequirement, strengthRequirement, dexterityRequirement, intelligenceRequirement);
            System.out.printf("Weight: %.1f, Armour: %.1f, Flexibility: %.3f\n", weight, armour, flexibility);
            secondaryStats.print();
        }
    }



    public static enum Slot {
        HEAD, CHEST, FULL_BODY, HANDS, FEET, LEGS, BELT, SMALL_SHIELD, LARGE_SHIELD, ONE_HANDED_WEAPON, TWO_HANDED_WEAPON, NONE,
        AMULET, RING     // others
    }

    // what is the primary base material?
    public static enum EquipmentType {
        PLATE, CHAINMAIL, LEATHER, CLOTH, WOOD, JEWELLERY, OTHER
    }

}

package jespo655.cc.Crafting.Materials;

import jespo655.cc.Crafting.Wrappers.CraftingMaterial;
import jespo655.cc.Crafting.Wrappers.Item;
import jespo655.cc.Crafting.Wrappers.SecondaryStats;
import jespo655.cc.Crafting.Wrappers.StackableItem;

/**
 * A crafting equipmentType has a base weaponType and other stats.
 *
 */
public class BaseMaterial implements StackableItem<Float>, CraftingMaterial {

    public static enum Type {
        WOOD, LEATHER, METAL, CLOTH      // base types
    }


    public String name;
    public Type type;
    public int level;
    public float weight;      // per unit
    public float flexibility; // [0, 1] high flexibility -> high agility
    public float toughness;   // high toughness -> high armour & high durability

    public final SecondaryStats secondaryStats = new SecondaryStats();

    public BaseMaterial() {}
    public BaseMaterial(Type type) {
        this.type = type;
    }
    public BaseMaterial(BaseMaterial m) {
        type = m.type;
        level = m.level;
        weight = m.weight;
        flexibility = m.flexibility;
        toughness = m.toughness;
        secondaryStats.healthRegeneration = m.secondaryStats.healthRegeneration;
        secondaryStats.movementSpeed = m.secondaryStats.movementSpeed;
        secondaryStats.elementalResistance = m.secondaryStats.elementalResistance;
        secondaryStats.manaRegeneration = m.secondaryStats.manaRegeneration;
        secondaryStats.strength = m.secondaryStats.strength;
        secondaryStats.dexterity = m.secondaryStats.dexterity;
        secondaryStats.intelligence = m.secondaryStats.intelligence;
    }



    private boolean isUsed = false;

    @Override
    public CraftingMaterial use() {
        isUsed = true;
        return this;
    }

    @Override
    public boolean isUsed() {
        return isUsed;
    }

    @Override
    public Item.Identifier getIdentifier() {
        return Item.Identifier.CRAFTING_MATERIAL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseMaterial)) return false;

        BaseMaterial material = (BaseMaterial) o;

        if (flexibility != material.flexibility) return false;
        if (level != material.level) return false;
        if (toughness != material.toughness) return false;
        if (weight != material.weight) return false;
        if (name != null ? !name.equals(material.name) : material.name != null) return false;
        if (!secondaryStats.equals(material.secondaryStats)) return false;
        if (type != material.type) return false;

        return true;
    }

    @Override
    public Class<Float> getQuantityClass() {
        return Float.class;
    }

    @Override
    public Float getMaxStackSize() {
        return Float.MAX_VALUE;
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public float getWeight() {
        return weight;
    }

    @Override
    public SecondaryStats getSecondaryStats() {
        return secondaryStats;
    }
}

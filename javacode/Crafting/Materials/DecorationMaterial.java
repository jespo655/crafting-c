package jespo655.cc.Crafting.Materials;

import jespo655.cc.Crafting.Wrappers.CraftingMaterial;
import jespo655.cc.Crafting.Wrappers.Item;
import jespo655.cc.Crafting.Wrappers.SecondaryStats;
import jespo655.cc.Crafting.Wrappers.StackableItem;

/**
 * A crafting equipmentType has a base weaponType and other stats.
 * Decoration materials are all kind of materials mostly used for decoration.
 *
 */
public class DecorationMaterial implements StackableItem<Integer>, CraftingMaterial {

    public static enum Type {
        STICK, STONE, GEM        // others
    }


    public String name;
    public Type type;
    public int level;
    public float weight;      // per unit
    public final SecondaryStats secondaryStats = new SecondaryStats();



    private boolean isUsed = false;

    @Override
    public CraftingMaterial use() {
        isUsed = true;
        return this;
    }

    @Override
    public boolean isUsed() {
        return isUsed;
    }

    @Override
    public Item.Identifier getIdentifier() {
        return Item.Identifier.CRAFTING_MATERIAL;
    }

    @Override
    public Class<Integer> getQuantityClass() {
        return Integer.class;
    }

    @Override
    public Integer getMaxStackSize() {
        return Integer.MAX_VALUE;
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public float getWeight() {
        return weight;
    }

    @Override
    public SecondaryStats getSecondaryStats() {
        return secondaryStats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DecorationMaterial)) return false;

        DecorationMaterial that = (DecorationMaterial) o;

        if (level != that.level) return false;
        if (weight != that.weight) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (!secondaryStats.equals(that.secondaryStats)) return false;
        if (type != that.type) return false;

        return true;
    }


}

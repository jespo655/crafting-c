package jespo655.cc.Crafting.Materials;


/**
 * The MaterialContainer contains a number of predefined materials that can be used in the game.
 * Custom materials can be created and used as well.
 *
 * Higher level materials should have about the same weight and flexibility (at least in the same area),
 *      but higher toughness and secondary stats
 */
public class MaterialContainer {


    /*
    public String name;
    public Type weaponType;
    public int level;
    public float weight;      // per unit
    public float flexibility; // [0, 1] low flexibility -> movement speed penalty (not on weapons)
    public float toughness;   // high toughness -> high armour / damage  (high durability?)

    public ItemStats secondaryStats;

    // str-based stats
    public int healthRegeneration;

    // dex-based stats
    public int movementSpeed;

    // int-based stats
    public int elementalResistance;
    public int manaRegeneration;

    // bonus main-stats
    public int strength;
    public int dexterity;
    public int intelligence;
    */


    public static final BaseMaterial LEATHER_TEMPLATE;
    public static final BaseMaterial METAL_TEMPLATE;
    public static final BaseMaterial CLOTH_TEMPLATE;
    public static final BaseMaterial WOOD_TEMPLATE;

    static {
        LEATHER_TEMPLATE = new BaseMaterial(BaseMaterial.Type.LEATHER);
        LEATHER_TEMPLATE.weight = 0.20f;
        LEATHER_TEMPLATE.flexibility = 0.70f;
        LEATHER_TEMPLATE.toughness = 0.40f;    // 2 * weight

        METAL_TEMPLATE = new BaseMaterial(BaseMaterial.Type.METAL);
        METAL_TEMPLATE.weight = 0.50f;
        METAL_TEMPLATE.flexibility = 0.20f;
        METAL_TEMPLATE.toughness = 1.00f;      // 2 * weight

        CLOTH_TEMPLATE = new BaseMaterial(BaseMaterial.Type.CLOTH);
        CLOTH_TEMPLATE.weight = 0.10f;
        CLOTH_TEMPLATE.flexibility = 0.90f;
        CLOTH_TEMPLATE.toughness = 0.10f;

        WOOD_TEMPLATE = new BaseMaterial(BaseMaterial.Type.WOOD);
        WOOD_TEMPLATE.weight = 0.30f;
        WOOD_TEMPLATE.flexibility = 0.40f;
        WOOD_TEMPLATE.toughness = 0.60f;



    }






    private static MaterialContainer ourInstance = new MaterialContainer();

    public static MaterialContainer getInstance() {
        return ourInstance;
    }

    private MaterialContainer() {
    }

}

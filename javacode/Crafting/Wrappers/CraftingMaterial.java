package jespo655.cc.Crafting.Wrappers;

/**
 * CraftingMaterial is anything that can be used when crafting other items.
 * This can even include other crafted items.
 *
 * Only items that implement this interface can be attempted to be crafted from.
 */
public interface CraftingMaterial extends Item {

    public CraftingMaterial use();  // return the used material
    public boolean isUsed();

    public int getLevel();
    public float getWeight();                 // per unit
    public SecondaryStats getSecondaryStats();   // per unit

}

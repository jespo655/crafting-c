package jespo655.cc.Crafting.Wrappers;

/**
 * The ItemStats class contains information about stats on materials and crafted items.
 */
public class SecondaryStats {

    // str-based stats
    public float healthRegeneration;

    // dex-based stats
    public float movementSpeed;

    // int-based stats
    public float elementalResistance;
    public float manaRegeneration;

    // bonus main-stats
    public float strength;
    public float dexterity;
    public float intelligence;


    @Override
    public SecondaryStats clone() {
        SecondaryStats is = new SecondaryStats();
        is.healthRegeneration = healthRegeneration;
        is.movementSpeed = movementSpeed;
        is.elementalResistance = elementalResistance;
        is.manaRegeneration = manaRegeneration;
        is.strength = strength;
        is.dexterity = dexterity;
        is.intelligence = intelligence;
        return is;
    }

    public void copyFrom(SecondaryStats is) {
        healthRegeneration = is.healthRegeneration;
        movementSpeed = is.movementSpeed;
        elementalResistance = is.elementalResistance;
        manaRegeneration = is.manaRegeneration;
        strength = is.strength;
        dexterity = is.dexterity;
        intelligence = is.intelligence;
    }
    
    
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SecondaryStats)) return false;

        SecondaryStats secondaryStats = (SecondaryStats) o;

        if (dexterity != secondaryStats.dexterity) return false;
        if (elementalResistance != secondaryStats.elementalResistance) return false;
        if (healthRegeneration != secondaryStats.healthRegeneration) return false;
        if (intelligence != secondaryStats.intelligence) return false;
        if (manaRegeneration != secondaryStats.manaRegeneration) return false;
        if (movementSpeed != secondaryStats.movementSpeed) return false;
        if (strength != secondaryStats.strength) return false;

        return true;
    }


    public void print() {
        System.out.printf("life regen: %.1f, movement speed: %.1f\n",healthRegeneration,movementSpeed);
        System.out.printf("mana regen: %.1f, elemental resist: %.1f\n",manaRegeneration,elementalResistance);
        System.out.printf("strength: %.1f, dexterity: %.1f, intelligence: %.1f\n",strength,dexterity,intelligence);
    }

}

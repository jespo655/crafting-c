package jespo655.cc.Crafting.Wrappers;

/**
 * Any item that implements Stackable<E> can be stored in an ItemStack<E>.
 */
public interface StackableItem<N extends Number> extends Item {

    // TODO: implement getMaxStackSize() & use it when combining stacks to create one full stack and one with the leftovers
    public abstract N getMaxStackSize();
    public abstract Class<N> getQuantityClass();


}

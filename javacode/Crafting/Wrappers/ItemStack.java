package jespo655.cc.Crafting.Wrappers;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * The MaterialStack contains a certain quantity of one item.
 * It can be split into several stacks, or several stacks can be combined to one.
 * If the quantity reaches 0 it should be removed.
 *
 */
public class ItemStack<N extends Number> implements Item, CraftingMaterial {

    private final Class<N> quantityClass;
    private N quantity;
    public final StackableItem<N> item;


    public N getQuantity() {
        return quantity;
    }

    public N getMaxStackSize() {
        return item.getMaxStackSize();
    }

    public String getQuantityString() {
        if (isDiscrete(quantity)) {
            return String.format("%d",quantity.intValue());
        } else {
            return String.format("%.2f",quantity.floatValue());
        }
    }

    public boolean isEmpty() {
        return quantity.floatValue() <= 0;
    }


    public ItemStack (StackableItem<N> item, Number quantity) {
        quantityClass = item.getQuantityClass();
        this.item = item;
        this.quantity = convertToN(quantity);
    }



    /** ---------------------------------------------------------------------------------------------------------
     *
     *            Usage as item
     *
     * ----------------------------------------------------------------------------------------------------------
     */

    @Override
    public Item.Identifier getIdentifier() {
        return item.getIdentifier();
    }




    /** ---------------------------------------------------------------------------------------------------------
     *
     *            Usage as crafting material
     *
     * ----------------------------------------------------------------------------------------------------------
     */

    @Override
    public CraftingMaterial use() {
        return use(1);
    }

    public CraftingMaterial use(Number amount) {
        return split(amount);
    }

    @Override
    public boolean isUsed() {
        return isEmpty();
    }

    @Override
    public int getLevel() {
        if (item instanceof CraftingMaterial)
            return ((CraftingMaterial) item).getLevel();
        else
            return 0;
    }

    @Override
    public float getWeight() {
        if (item instanceof CraftingMaterial)
            return ((CraftingMaterial) item).getWeight();
        else
            return 0;
    }

    @Override
    public SecondaryStats getSecondaryStats() {
        if (item instanceof CraftingMaterial)
            return ((CraftingMaterial) item).getSecondaryStats();
        else
            return new SecondaryStats();
    }





    /** ---------------------------------------------------------------------------------------------------------
     *
     *            Stack size modification
     *
     * ----------------------------------------------------------------------------------------------------------
     */

    // move as many items as possible from another stack to this one.
    // return true if any items was moved.
    public boolean combine (ItemStack is) {
        // move as much as possible from that stack to this one
        // can only combine stacks of equal items
        if (is != this && item.equals(is.item)) {

            N totalQuantity = addN(quantity, is.quantity);
            N maxQuantity = item.getMaxStackSize();
            if (totalQuantity.floatValue() > maxQuantity.floatValue()) {
                is.quantity = subtractN(totalQuantity, maxQuantity);
                quantity = maxQuantity;
            } else {
                quantity = totalQuantity;
                is.quantity = convertToN(0);
            }
            return true;
        }
        return false;
    }


    // move the specified amount of items to a new stack
    // if amount >= size, then move all items
    public ItemStack<N> split(Number amount) {
        N amountN = convertToN(amount);

        // Remove the specified quantity from this container and put it in a new one.
        ItemStack<N> is = new ItemStack<N>(item, convertToN(0));
        if (amount.doubleValue() > 0) {
            // should move something to the new container
            if (amount.doubleValue() < quantity.doubleValue()) {
                // should not move everything to the new container
                is.quantity = amountN;
                quantity = subtractN(quantity, amountN);
            } else {
                // should move everything to the new container
                is.quantity = quantity;
                quantity = convertToN(0);
            }
        }
        return is;
    }


    // like split, but does not subtract the amount from this stack
    public ItemStack<N> clone(Number amount) {
        return new ItemStack<N>(item, convertToN(amount));
    }



    // add this stack to a list that could contain other item stacks
    public boolean addToList(List<? super ItemStack> list) {
        // go through the list and search for stacks to combine with
        // also, if this already is in the list, don't add it again
        // returns true if added materials to the list
        // returns false if this stack was already in the list
        for (Object o : list) {

            if (o == this)
                return false;

            if (o instanceof ItemStack) {
                ItemStack is = (ItemStack) o;

                is.combine(this);

                if (isEmpty())
                    return true;
            }
        }
        list.add(this);
        return true;
    }



    /** ---------------------------------------------------------------------------------------------------------
     *
     *            Helper functions
     *
     * ----------------------------------------------------------------------------------------------------------
     */


    private static boolean isDiscrete(Number n) {
        return n.longValue() == n.doubleValue();
    }

    // n1 + n2
    private N addN(Number n1, Number n2) {
        if (quantityClass == Long.class || quantityClass == Integer.class || quantityClass == Short.class)
            return convertToN(n1.longValue() + n2.longValue());
        else
            return convertToN(n1.doubleValue() + n2.doubleValue());
    }

    // n1 - n2
    private N subtractN(Number n1, Number n2) {
        if (quantityClass == Long.class || quantityClass == Integer.class || quantityClass == Short.class)
            return convertToN(n1.longValue() - n2.longValue());
        else
            return convertToN(n1.doubleValue() - n2.doubleValue());
    }


    private <I> N convertToN(I input) {
        N output = null;
        try {
            if (input != null) {
                try {
                    output = quantityClass.getConstructor(String.class).newInstance(input.toString());
                } catch (NumberFormatException | InvocationTargetException e) {
                    // this happens if we input a decimal number when N is a discrete weaponType
                    // try to split the string into discrete and decimal part and only use the discrete part
                    String discreteNumberString = input.toString().split("\\.")[0];
                    output = quantityClass.getConstructor(String.class).newInstance(discreteNumberString);
                }
            }
            else
                output = quantityClass.getConstructor(String.class).newInstance("0");
        } catch (Exception e) {
            System.out.println("convertToN exception: "+e);
        }
        return output;
    }




}

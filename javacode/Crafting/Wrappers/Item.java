package jespo655.cc.Crafting.Wrappers;

import jespo655.cc.Crafting.Items.Equipment;
import jespo655.cc.Crafting.Items.Weapon;

/**
 * An item is anything
 */
public interface Item {
    /**
     * The ItemIdentifier enum contains all types of item that can possibly exist in the game and is used among other things for identification of items used in crafting
     */

    public Identifier getIdentifier();

    /*
    public Item(Identifier identifier) {
        this.identifier = identifier;
    }

    private Identifier identifier;
    public Identifier getIdentifier() {
        return identifier;
    }
    public Number getQuantity() {
        return 1;
    }
    public String getQuantityString() {
        return "1";
    }
*/

    public static enum Identifier {
        // slots: HEAD, CHEST, HANDS, FEET, LEGS, BELT, SMALL_SHIELD, LARGE_SHIELD
        // materials: LEATHER, PLATE, CHAINMAIL, CLOTH, WOOD

        // leather
        LEATHER_HELMET,
        LEATHER_CHEST,
        LEATHER_FULL_BODY,
        LEATHER_GLOVES,
        LEATHER_BOOTS,
        LEATHER_LEGS,
        LEATHER_BELT,
        LEATHER_SMALL_SHIELD,
        LEATHER_LARGE_SHIELD,

        // metal
        PLATE_HELMET,
        PLATE_CHEST,
        PLATE_FULL_BODY,
        PLATE_GLOVES,
        PLATE_BOOTS,
        PLATE_LEGS,
        PLATE_BELT,
        PLATE_SMALL_SHIELD,
        PLATE_LARGE_SHIELD,

        CHAINMAIL_HELMET,
        CHAINMAIL_CHEST,
        CHAINMAIL_FULL_BODY,
        CHAINMAIL_GLOVES,
        CHAINMAIL_BOOTS,
        CHAINMAIL_LEGS,
        CHAINMAIL_BELT,
        CHAINMAIL_SMALL_SHIELD,
        CHAINMAIL_LARGE_SHIELD,

        // cloth
        CLOTH_HELMET,
        CLOTH_CHEST,
        CLOTH_FULL_BODY,
        CLOTH_GLOVES,
        CLOTH_BOOTS,
        CLOTH_LEGS,
        CLOTH_BELT,
        CLOTH_SMALL_SHIELD,
        CLOTH_LARGE_SHIELD,

        // wood
        WOODEN_HELMET,
        WOODEN_CHEST,
        WOODEN_FULL_BODY,
        WOODEN_GLOVES,
        WOODEN_BOOTS,
        WOODEN_LEGS,
        WOODEN_BELT,
        WOODEN_SMALL_SHIELD,
        WOODEN_LARGE_SHIELD,

        // jewellery
        AMULET, RING,

        // weapons:
        DAGGER,
        SWORD_1H, SWORD_2H,
        BATTLE_AXE_1H, BATTLE_AXE_2H,
        ASSAULT_SPEAR_1H, ASSAULT_SPEAR_2H,
        POLEARM,
        WARHAMMER_1H, WARHAMMER_2H,
        FIST_WEAPON,
        BOW,
        STAFF,

        // others
        CRAFTING_MATERIAL,
        GOLD,
        UNKNOWN;




        public Equipment.Slot getSlot() {
            switch (this) {
                case LEATHER_HELMET:
                case PLATE_HELMET:
                case CHAINMAIL_HELMET:
                case CLOTH_HELMET:
                case WOODEN_HELMET:
                    return Equipment.Slot.HEAD;

                case LEATHER_CHEST:
                case PLATE_CHEST:
                case CHAINMAIL_CHEST:
                case CLOTH_CHEST:
                case WOODEN_CHEST:
                    return Equipment.Slot.CHEST;

                case LEATHER_FULL_BODY:
                case PLATE_FULL_BODY:
                case CHAINMAIL_FULL_BODY:
                case CLOTH_FULL_BODY:
                case WOODEN_FULL_BODY:
                    return Equipment.Slot.FULL_BODY;

                case LEATHER_GLOVES:
                case PLATE_GLOVES:
                case CHAINMAIL_GLOVES:
                case CLOTH_GLOVES:
                case WOODEN_GLOVES:
                    return Equipment.Slot.HANDS;

                case LEATHER_BOOTS:
                case PLATE_BOOTS:
                case CHAINMAIL_BOOTS:
                case CLOTH_BOOTS:
                case WOODEN_BOOTS:
                    return Equipment.Slot.FEET;

                case LEATHER_LEGS:
                case PLATE_LEGS:
                case CHAINMAIL_LEGS:
                case CLOTH_LEGS:
                case WOODEN_LEGS:
                    return Equipment.Slot.LEGS;

                case LEATHER_BELT:
                case PLATE_BELT:
                case CHAINMAIL_BELT:
                case CLOTH_BELT:
                case WOODEN_BELT:
                    return Equipment.Slot.BELT;

                case LEATHER_SMALL_SHIELD:
                case PLATE_SMALL_SHIELD:
                case CHAINMAIL_SMALL_SHIELD:
                case CLOTH_SMALL_SHIELD:
                case WOODEN_SMALL_SHIELD:
                    return Equipment.Slot.SMALL_SHIELD;

                case LEATHER_LARGE_SHIELD:
                case PLATE_LARGE_SHIELD:
                case CHAINMAIL_LARGE_SHIELD:
                case CLOTH_LARGE_SHIELD:
                case WOODEN_LARGE_SHIELD:
                    return Equipment.Slot.LARGE_SHIELD;

                case AMULET:
                    return Equipment.Slot.AMULET;

                case RING:
                    return Equipment.Slot.RING;

                case DAGGER:
                case SWORD_1H:
                case BATTLE_AXE_1H:
                case ASSAULT_SPEAR_1H:
                case WARHAMMER_1H:
                case FIST_WEAPON:
                    return Equipment.Slot.ONE_HANDED_WEAPON;

                case SWORD_2H:
                case BATTLE_AXE_2H:
                case ASSAULT_SPEAR_2H:
                case POLEARM:
                case WARHAMMER_2H:
                case BOW:
                case STAFF:
                    return Equipment.Slot.TWO_HANDED_WEAPON;

                default:
                    return Equipment.Slot.NONE;
            }
        }

        public Equipment.EquipmentType getEquipmentType() {
            switch (this) {
                case LEATHER_HELMET:
                case LEATHER_CHEST:
                case LEATHER_FULL_BODY:
                case LEATHER_GLOVES:
                case LEATHER_BOOTS:
                case LEATHER_LEGS:
                case LEATHER_BELT:
                case LEATHER_SMALL_SHIELD:
                case LEATHER_LARGE_SHIELD:
                    return Equipment.EquipmentType.LEATHER;

                case PLATE_HELMET:
                case PLATE_CHEST:
                case PLATE_FULL_BODY:
                case PLATE_GLOVES:
                case PLATE_BOOTS:
                case PLATE_LEGS:
                case PLATE_BELT:
                case PLATE_SMALL_SHIELD:
                case PLATE_LARGE_SHIELD:
                    return Equipment.EquipmentType.PLATE;

                case CHAINMAIL_HELMET:
                case CHAINMAIL_CHEST:
                case CHAINMAIL_FULL_BODY:
                case CHAINMAIL_GLOVES:
                case CHAINMAIL_BOOTS:
                case CHAINMAIL_LEGS:
                case CHAINMAIL_BELT:
                case CHAINMAIL_SMALL_SHIELD:
                case CHAINMAIL_LARGE_SHIELD:
                    return Equipment.EquipmentType.CHAINMAIL;

                case CLOTH_HELMET:
                case CLOTH_CHEST:
                case CLOTH_FULL_BODY:
                case CLOTH_GLOVES:
                case CLOTH_BOOTS:
                case CLOTH_LEGS:
                case CLOTH_BELT:
                case CLOTH_SMALL_SHIELD:
                case CLOTH_LARGE_SHIELD:
                    return Equipment.EquipmentType.CLOTH;

                case WOODEN_HELMET:
                case WOODEN_CHEST:
                case WOODEN_FULL_BODY:
                case WOODEN_GLOVES:
                case WOODEN_BOOTS:
                case WOODEN_LEGS:
                case WOODEN_BELT:
                case WOODEN_SMALL_SHIELD:
                case WOODEN_LARGE_SHIELD:
                    return Equipment.EquipmentType.WOOD;

                case AMULET:
                case RING:
                    return Equipment.EquipmentType.JEWELLERY;

                default:
                    return Equipment.EquipmentType.OTHER;
            }
        }

        public Weapon.Type getWeaponType() {
            switch (this) {
                case DAGGER: return Weapon.Type.DAGGER;
                case SWORD_1H: return Weapon.Type.SWORD_1H;
                case SWORD_2H: return Weapon.Type.SWORD_2H;
                case BATTLE_AXE_1H: return Weapon.Type.BATTLE_AXE_1H;
                case BATTLE_AXE_2H: return Weapon.Type.BATTLE_AXE_2H;
                case ASSAULT_SPEAR_1H: return Weapon.Type.ASSAULT_SPEAR_1H;
                case ASSAULT_SPEAR_2H: return Weapon.Type.ASSAULT_SPEAR_2H;
                case POLEARM: return Weapon.Type.POLEARM;
                case WARHAMMER_1H: return Weapon.Type.WARHAMMER_1H;
                case WARHAMMER_2H: return Weapon.Type.WARHAMMER_2H;
                case FIST_WEAPON: return Weapon.Type.FIST_WEAPON;
                case BOW: return Weapon.Type.BOW;
                case STAFF: return Weapon.Type.STAFF;
                default: return Weapon.Type.NONE;
            }
        }

        public boolean isWeapon() {
            return getWeaponType() != Weapon.Type.NONE;
        }

        public boolean isEquipment() {
            return this != CRAFTING_MATERIAL && this != UNKNOWN;
        }




    }
}

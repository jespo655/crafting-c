#include "random.h"
#include <cstdlib>
#include <ctime>


namespace Random {

	static bool inited = false;

	void init()
	{
		std::srand(static_cast<unsigned>(std::time(0)));
		inited = true;
	}



	// [0,1]
	float next_float()
	{
		if (!inited) init();
		return static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX);
	}

	// [0,max]
	float next_float(float max)
	{
		if (!inited) init();
		return static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX/max);
	}

	// [min,max]
	float next_float(float min, float max)
	{
		if (!inited) init();
		return min + static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX/(max-min));
	}



 	// [0,INT_MAX)
	int next_int()
	{
		if (!inited) init();
		return std::rand();
	}

	// [0,max)
	int next_int(int max)
	{
		return next_int() % max;
	}

	// [min,max)
	int next_int(int min, int max)
	{
		return min + next_int(max-min);
	}



}
// si_units.h
// Jesper Post, 2015-10-04
//
// Provides a strongly typed representation of the SI system.
//
// Usage:
// namespace SI has the basic functionality. Allows for user defined unit and value types.
// use 'using U = Unit<M,K,S>' to define a unit, or the macro 'UNIT(Unit_name, M, K, S, suffix)' to define a unit with a suffix literal
// use 'using V = Value<Unit>' to define a value type that uses a specific unit.
//
// namespace SI::Base includes the units Meter, Kilogram, Second and Dimensionless as well as some simple Value types.
// namespace SI::Derived includes more complex derived types such as Force (Newton).
//
// Other namespaces with more unit and value types:
// namespace SI::Electric
// namespace SI::Temperature
// namespace SI::Radioactive
// namespace SI::Light
// namespace SI::Chemical
//
// To use prefixed values, use namespace SI::Prefixes and the macro PREFIXED_VALUE(Value_name, Exponent, Unit, suffix) to define a value.


#pragma once

#include <iostream>
#include <cmath>	// pow for SI::Prefixes

namespace SI {

	// SI_unit: a unit in the SI system
	template<int M, int Kg, int S, int A, int K, int Mol, int Cd>
	struct SI_unit {
		enum { m=M, kg=Kg, s=S, a=A, k=K, mol=Mol, cd=Cd};
	};

	// Unit: a unit in the MKS system
	template<int M, int Kg, int S>
	using Unit = SI_unit<M, Kg, S, 0, 0, 0, 0>;



	// Value: a magnitude of a unit
	template<typename U>
	struct Value {
		double val;
		constexpr explicit Value(double d) : val{d} {}
		constexpr explicit Value(int i) : val{static_cast<double>(i)} {}
		constexpr Value(const Value<U>& v) = default;
		constexpr Value() = default;

		Value<U>& operator+=(const Value<U>& rhs)
		{
			val += rhs.val;
			return *this;
		}

		Value<U>& operator-=(const Value<U>& rhs)
		{
			val -= rhs.val;
			return *this;
		}

		Value<U>& operator*=(const Value<Unit<0,0,0>>& rhs)
		{
			val *= rhs.val;
			return *this;
		}

		Value<U>& operator/=(const Value<Unit<0,0,0>>& rhs)
		{
			val /= rhs.val;
			return *this;
		}

		Value<U>& operator*=(const double& rhs)
		{
			val *= rhs;
			return this;
		}

		Value<U>& operator/=(const double& rhs)
		{
			val /= rhs;
			return this;
		}

		bool operator<(const Value<U>& rhs) const { return val<rhs.val; }
		bool operator>(const Value<U>& rhs) const { return val>rhs.val; }
		bool operator<=(const Value<U>& rhs) const { return val<=rhs.val; }
		bool operator>=(const Value<U>& rhs) const { return val>=rhs.val; }
		bool operator==(const Value<U>& rhs) const { return val==rhs.val; }
		bool operator!=(const Value<U>& rhs) const { return val!=rhs.val; }

	};



	template<int M1, int M2, int Kg1, int Kg2, int S1, int S2, int A1, int A2, int K1, int K2, int Mol1, int Mol2, int Cd1, int Cd2>
	Value<SI_unit<M1+M2, Kg1+Kg2, S1+S2, A1+A2, K1+K2, Mol1+Mol2, Cd1+Cd2>>
		operator*(const Value<SI_unit<M1,Kg1,S1,A1,K1,Mol1,Cd1>>& lhs,
				  const Value<SI_unit<M2,Kg2,S2,A2,K2,Mol2,Cd2>>& rhs)
	{
		return Value<SI_unit<M1+M2, Kg1+Kg2, S1+S2, A1+A2, K1+K2, Mol1+Mol2, Cd1+Cd2>>{lhs.val * rhs.val};
	}

	template<int M1, int M2, int Kg1, int Kg2, int S1, int S2, int A1, int A2, int K1, int K2, int Mol1, int Mol2, int Cd1, int Cd2>
	Value<SI_unit<M1-M2, Kg1-Kg2, S1-S2, A1-A2, K1-K2, Mol1-Mol2, Cd1-Cd2>>
		operator/(const Value<SI_unit<M1,Kg1,S1,A1,K1,Mol1,Cd1>>& lhs,
				  const Value<SI_unit<M2,Kg2,S2,A2,K2,Mol2,Cd2>>& rhs)
	{
		return Value<SI_unit<M1-M2, Kg1-Kg2, S1-S2, A1-A2, K1-K2, Mol1-Mol2, Cd1-Cd2>>{lhs.val / rhs.val};
	}


	template<typename Unit>
	Value<Unit> operator*(const Value<Unit>& lhs, const double& rhs)
	{
		return Value<Unit>{lhs.val * rhs};
	}

	template<typename Unit>
	Value<Unit> operator*(const double& lhs, const Value<Unit>& rhs)
	{
		return Value<Unit>{lhs * rhs.val};
	}

	template<typename Unit>
	Value<Unit> operator/(const Value<Unit>& lhs, const double& rhs)
	{
		return Value<Unit>{lhs.val / rhs};
	}

	template<typename Unit>
	Value<Unit> operator/(const double& lhs, const Value<Unit>& rhs)
	{
		return Value<Unit>{lhs / rhs.val};
	}




	template<typename Unit>
	Value<Unit> operator+(const Value<Unit>& lhs, const Value<Unit>& rhs)
	{
		return Value<Unit>{lhs.val + rhs.val};
	}

	template<typename Unit>
	Value<Unit> operator-(const Value<Unit>& lhs, const Value<Unit>& rhs)
	{
		return Value<Unit>{lhs.val - rhs.val};
	}


	inline Value<Unit<0,0,0>> operator+(const Value<Unit<0,0,0>>& lhs, const double& rhs)
	{
		return Value<Unit<0,0,0>>{lhs.val+rhs};
	}


	inline Value<Unit<0,0,0>> operator+(const double& lhs, const Value<Unit<0,0,0>>& rhs)
	{
		return Value<Unit<0,0,0>>{lhs+rhs.val};
	}



	template<typename Unit>
	Value<Unit> operator-(const Value<Unit>& v)
	{
		return Value<Unit>{-v.val};
	}





	template<typename U>
	std::istream& operator>>(std::istream& is, Value<U>& v) {
		is >> v.val;
		return is;
	}

	template<typename U>
	std::ostream& operator<<(std::ostream& os, const Value<U>& v) {
		os << v.val << U();
		return os;
	}


	template<int M, int Kg, int S, int A, int K, int Mol, int Cd>
	std::ostream& operator<<(std::ostream& os, const SI_unit<M,Kg,S,A,K,Mol,Cd>&) {

		// // KgMS notation
		// if (Kg != 0) {
		// 	os << "kg";
		// 	if (Kg != 1) os << Kg;
		// }
		// if (M != 0) {
		// 	os << "m";
		// 	if (M != 1) os << M;
		// }
		// if (S != 0) {
		// 	os << "s";
		// 	if (S != 1) os << S;
		// }
		// if (A != 0) {
		// 	os << "A";
		// 	if (A != 1) os << A;
		// }
		// if (K != 0) {
		// 	os << "K";
		// 	if (K != 1) os << K;
		// }
		// if (Mol != 0) {
		// 	os << "mol";
		// 	if (Mol != 1) os << Mol;
		// }
		// if (Cd != 0) {
		// 	os << "cd";
		// 	if (Cd != 1) os << Cd;
		// }

		// positive/negative notation
		bool has_positive = M>0 || Kg>0 || S>0 || A>0 || K>0 || Mol>0 || Cd>0;
		bool has_negative = M<0 || Kg<0 || S<0 || A<0 || K<0 || Mol<0 || Cd<0;

		if (has_positive || has_negative)
			// leading space before the unit
			os << " ";

		if (has_positive) {
			// write positive numbers
			if (Kg > 0) os << "kg";
			if (Kg > 1) os << Kg;
			if (M > 0) os << "m";
			if (M > 1) os << M;
			if (S > 0) os << "s";
			if (S > 1) os << S;
			if (A > 0) os << "A";
			if (A > 1) os << A;
			if (K > 0) os << "K";
			if (K > 1) os << K;
			if (Mol > 0) os << "mol";
			if (Mol > 1) os << Mol;
			if (Cd > 0) os << "cd";
			if (Cd > 1) os << Cd;
		} else if (has_negative)
			os << "1";

		if (has_negative) {
			// write negative numbers
			os << "/";
			if (Kg < 0) os << "kg";
			if (Kg < -1) os << -Kg;
			if (M < 0) os << "m";
			if (M < -1) os << -M;
			if (S < 0) os << "s";
			if (S < -1) os << -S;
			if (A < 0) os << "A";
			if (A < -1) os << -A;
			if (K < 0) os << "K";
			if (K < -1) os << -K;
			if (Mol < 0) os << "mol";
			if (Mol < -1) os << -Mol;
			if (Cd < 0) os << "cd";
			if (Cd < -1) os << -Cd;
		}
		return os;
	}








	#define SI_UNIT(Unit_name, M, Kg, S, A, K, Mol, Cd, suffix)					\
	using Unit_name = SI_unit<M,Kg,S,A,K,Mol,Cd>;								\
	constexpr Value<Unit_name>operator""_ ## suffix(long double d)				\
	{ return Value<Unit_name>{static_cast<double>(d)}; }						\
	constexpr Value<Unit_name>operator""_ ## suffix(unsigned long long int i)	\
	{ return Value<Unit_name>{static_cast<double>(i)}; }


	#define UNIT(Unit_name, M, K, S, suffix)				\
	SI_UNIT(Unit_name, M, K, S, 0, 0, 0, 0, suffix);



	namespace Base {

		using namespace SI;

		// base units
		UNIT(Dimensionless, 0, 0, 0, );
		UNIT(Meter, 1, 0, 0, m);
		UNIT(Kilogram, 0, 1, 0, kg);
		UNIT(Second, 0, 0, 1, s);

		using Length = Value<Meter>;
		using Mass = Value<Kilogram>;
		using Time = Value<Second>;

		using Velocity = Value<Unit<1,0,-1>>;
		using Acceleration = Value<Unit<1,0,-2>>;

		using Frequency = Value<Unit<0,0,-1>>;

		using Area = Value<Unit<2,0,0>>;
		using Volume = Value<Unit<3,0,0>>;

		using Angular_momentum = Value<Unit<2,1,-1>>;

	}

	namespace Derived {

		using namespace SI;

		// derived units
		UNIT(Radian, 0, 0, 0, rad);		// m/m
		UNIT(Steradian, 0, 0 ,0, sr);	// m2/m2
		UNIT(Newton, 1, 1, -2, N);
		UNIT(Pascal, -1, 1, -2, Pa);
		UNIT(Joule, 2, 1, -2, J);
		UNIT(Watt, 2, 1, -3, W);

		using Angle = Value<Radian>;
		using Solid_angle = Value<Steradian>;

		using Force = Value<Newton>;
		using Pressure = Value<Pascal>;
		using Energy = Value<Joule>;
		using Power = Value<Watt>;

	}

	namespace Electric {

		using namespace SI;

		// units using ampere
		SI_UNIT(Ampere, 0, 0, 0, 1, 0, 0, 0, A);
		SI_UNIT(Coulomb, 0, 0, 1, 1, 0, 0, 0, C);
		SI_UNIT(Volt, 2, 1, -3, -1, 0, 0, 0, V);		// W/A
		SI_UNIT(Farad, -2, -1, 4, 2, 0, 0, 0, F);		// C/V
		SI_UNIT(Ohm, -2, 1, 3, 2, 0, 0, 0, O);			// V/A
		SI_UNIT(Siemens, 2, -1, -3, -2, 0, 0, 0, S); 	// A/V
		SI_UNIT(Weber, 2, 1, -2, -1, 0, 0, 0, Wb);		// Vs
		SI_UNIT(Tesla, 0, 1, -2, -1, 0, 0, 0, T);		// Wb/m2
		SI_UNIT(Henry, 2, 1, -2, -2, 0, 0, 0, H); 		// Wb/A

		using Electric_current = Value<Ampere>;
		using Electric_charge = Value<Coulomb>;
		using Voltage = Value<Volt>;
		using Electric_capacitance = Value<Farad>;
		using Electric_resistance = Value<Ohm>;
		using Electric_conductance = Value<Siemens>;
		using Magnetic_flux = Value<Weber>;
		using Magnetic_field_strength = Value<Tesla>;
		using Inductance = Value<Henry>;

	}

	namespace Temperature {

		using namespace SI;

		SI_UNIT(Kelvin, 0, 0, 0, 0, 1, 0, 0, K);

		using Temperature = Value<Kelvin>;

	}

	namespace Radioactive {

		using namespace SI;

		UNIT(Bequerel, 0, 0, -1, Bq);
		UNIT(Gray, 2, 0, -2, Gy);		// J/Kg
		UNIT(Sievert, 2, 0, -2, Sv);	// J/Kg

		using Radioactivity = Value<Bequerel>;
		using Absorbed_dose = Value<Gray>;
		using Equivalent_dose = Value<Sievert>;

		// others: Bequerel, Gray, Sievert

	}

	namespace Light {

		using namespace SI;

		SI_UNIT(Candela, 0, 0, 0, 0, 0, 1, 0, cd);
		SI_UNIT(Lumen, 0, 0, 0, 0, 0, 1, 0, lm);	// cd*sr
		SI_UNIT(Lux, -2, 0, 0, 0, 0, 1, 0, lx);			// lm/m2

		using Luminous_intensity = Value<Candela>;
		using Luminous_flux = Value<Lumen>;
		using Illuminance = Value<Lux>;

	}

	namespace Chemical {

		using namespace SI;

		SI_UNIT(Mole, 0, 0, 0, 0, 0, 0, 1, mol);
		SI_UNIT(Katal, 0, 0, -1, 0, 0, 0, 1, kat);

		using Substance_amount = Value<Mole>;
		using Catalytic_activity = Value<Katal>;
	}


	namespace All {
		using namespace SI;
		using namespace Base;
		using namespace Derived;
		using namespace Electric;
		using namespace Temperature;
		using namespace Radioactive;
		using namespace Light;
		using namespace Chemical;
	}



	namespace Prefixes {

		using namespace SI;

		enum { deca=1, hecto=2, kilo=3, mega=6, giga=9, tera=12, peta=15, exa=18, zetta=21, yotta=24,
			deci=-1, centi=-2, milli=-3, micro=-6, nano=-9, pico=-12, femto=-15, atto=-18, zepto=-21, yocto=-24 };

		template<int E, typename U>
		struct Prefixed_value {
			enum { e=E };
			double val;

			constexpr Prefixed_value() = default;
			constexpr explicit Prefixed_value(double d) : val{d} {}
			constexpr explicit Prefixed_value(int i) : val{static_cast<double>(i)} {}

			template<int E2>
			constexpr Prefixed_value(const Prefixed_value<E2, U>& v) : val{v.val*std::pow(10,E2-E)} {}

			constexpr Prefixed_value(const Value<U>& v) : val{v.val*std::pow(10,-E)} {}		// conversion from Value<U>
			operator Value<U>() const { return Value<U>{val*std::pow(10,E)}; }				// conversion to Value<U>


			Prefixed_value<E,U>& operator*=(const Value<Unit<0,0,0>>& rhs)
			{
				val *= rhs.val;
				return *this;
			}

			Prefixed_value<E,U>& operator/=(const Value<Unit<0,0,0>>& rhs)
			{
				val /= rhs.val;
				return *this;
			}

			Prefixed_value<E,U>& operator+=(const Value<U>& rhs)
			{
				val += rhs.val*std::pow(10,-E);
				return *this;
			}

			Prefixed_value<E,U>& operator-=(const Value<U>& rhs)
			{
				val -= rhs.val*std::pow(10,-E);
				return *this;
			}

			bool operator<(const Prefixed_value<E,U>& rhs) const { return val<rhs.val; }
			bool operator>(const Prefixed_value<E,U>& rhs) const { return val>rhs.val; }
			bool operator<=(const Prefixed_value<E,U>& rhs) const { return val<=rhs.val; }
			bool operator>=(const Prefixed_value<E,U>& rhs) const { return val>=rhs.val; }
			bool operator==(const Prefixed_value<E,U>& rhs) const { return val==rhs.val; }
			bool operator!=(const Prefixed_value<E,U>& rhs) const { return val!=rhs.val; }

		};


		#define PREFIXED_VALUE(Value_name, Exp, Unit, suffix)					\
		using Value_name = Prefixed_value<Exp, Unit>;							\
		constexpr Value_name operator""_ ## suffix(long double d)				\
		{ return Value_name{static_cast<double>(d)}; }							\
		constexpr Value_name operator""_ ## suffix(unsigned long long int i)	\
		{ return Value_name{static_cast<double>(i)}; }



		template<int E, typename U>
		std::ostream& operator<<(std::ostream& os, const Prefixed_value<E,U>& pv)
		{
			os << pv.val;
			if (E != 0)
				os << "e" << E;
			os << U();
			return os;
		}

		template<int E, typename U>
		std::istream& operator>>(std::istream& is, Prefixed_value<E,U>& pv)
		{
			is >> pv.val;
			return is;
		}


		// addition
		template<int E1, int E2, typename Unit>
		Value<Unit> operator+(const Prefixed_value<E1, Unit>& lhs, const Prefixed_value<E2, Unit>& rhs)
		{ return static_cast<Value<Unit>>(lhs) + static_cast<Value<Unit>>(rhs); }

		template<int E, typename Unit>
		Value<Unit> operator+(const Value<Unit>& lhs, const Prefixed_value<E, Unit>& rhs)
		{ return Value<Unit>{lhs + static_cast<Value<Unit>>(rhs)}; }

		template<int E, typename Unit>
		Value<Unit> operator+(const Prefixed_value<E, Unit>& lhs, const Value<Unit>& rhs)
		{ return rhs + lhs;	}


		// subtraction
		template<int E1, int E2, typename Unit>
		Value<Unit> operator-(const Prefixed_value<E1, Unit>& lhs, const Prefixed_value<E2, Unit>& rhs)
		{ return static_cast<Value<Unit>>(lhs) - static_cast<Value<Unit>>(rhs); }

		template<int E, typename Unit>
		Value<Unit> operator-(const Value<Unit>& lhs, const Prefixed_value<E, Unit>& rhs)
		{ return Value<Unit>{lhs - static_cast<Value<Unit>>(rhs)}; }

		template<int E, typename Unit>
		Value<Unit> operator-(const Prefixed_value<E, Unit>& lhs, const Value<Unit>& rhs)
		{ return rhs - lhs;	}



		// multiplication
		template<int E1, int E2, int M1, int M2, int Kg1, int Kg2, int S1, int S2, int A1, int A2, int K1, int K2, int Mol1, int Mol2, int Cd1, int Cd2>
		Prefixed_value<E1+E2, SI_unit<M1+M2, Kg1+Kg2, S1+S2, A1+A2, K1+K2, Mol1+Mol2, Cd1+Cd2>>
			operator*(const Prefixed_value<E1, SI_unit<M1,Kg1,S1,A1,K1,Mol1,Cd1>>& lhs,
					  const Prefixed_value<E2, SI_unit<M2,Kg2,S2,A2,K2,Mol2,Cd2>>& rhs)
		{
			return Prefixed_value<E1+E2, SI_unit<M1+M2, Kg1+Kg2, S1+S2, A1+A2, K1+K2, Mol1+Mol2, Cd1+Cd2>>(lhs.val*rhs.val);
		}

		template<int E, int M1, int M2, int Kg1, int Kg2, int S1, int S2, int A1, int A2, int K1, int K2, int Mol1, int Mol2, int Cd1, int Cd2>
		Prefixed_value<E, SI_unit<M1+M2, Kg1+Kg2, S1+S2, A1+A2, K1+K2, Mol1+Mol2, Cd1+Cd2>>
			operator*(const Prefixed_value<E, SI_unit<M1,Kg1,S1,A1,K1,Mol1,Cd1>>& lhs,
					  const Value<SI_unit<M2,Kg2,S2,A2,K2,Mol2,Cd2>>& rhs)
		{
			return Prefixed_value<E, SI_unit<M1+M2, Kg1+Kg2, S1+S2, A1+A2, K1+K2, Mol1+Mol2, Cd1+Cd2>>(lhs.val*rhs.val);
		}

		template<int E, int M1, int M2, int Kg1, int Kg2, int S1, int S2, int A1, int A2, int K1, int K2, int Mol1, int Mol2, int Cd1, int Cd2>
		Prefixed_value<E, SI_unit<M1+M2, Kg1+Kg2, S1+S2, A1+A2, K1+K2, Mol1+Mol2, Cd1+Cd2>>
			operator*(const Value<SI_unit<M2,Kg2,S2,A2,K2,Mol2,Cd2>>& lhs,
					  const Prefixed_value<E, SI_unit<M1,Kg1,S1,A1,K1,Mol1,Cd1>>& rhs)
		{
			return Prefixed_value<E, SI_unit<M1+M2, Kg1+Kg2, S1+S2, A1+A2, K1+K2, Mol1+Mol2, Cd1+Cd2>>(lhs.val*rhs.val);
		}


		// division
		template<int E1, int E2, int M1, int M2, int Kg1, int Kg2, int S1, int S2, int A1, int A2, int K1, int K2, int Mol1, int Mol2, int Cd1, int Cd2>
		Prefixed_value<E1-E2, SI_unit<M1-M2, Kg1-Kg2, S1-S2, A1-A2, K1-K2, Mol1-Mol2, Cd1-Cd2>>
			operator/(const Prefixed_value<E1, SI_unit<M1,Kg1,S1,A1,K1,Mol1,Cd1>>& lhs,
					  const Prefixed_value<E2, SI_unit<M2,Kg2,S2,A2,K2,Mol2,Cd2>>& rhs)
		{
			return Prefixed_value<E1-E2, SI_unit<M1-M2, Kg1-Kg2, S1-S2, A1-A2, K1-K2, Mol1-Mol2, Cd1-Cd2>>(lhs.val/rhs.val);
		}

		template<int E, int M1, int M2, int Kg1, int Kg2, int S1, int S2, int A1, int A2, int K1, int K2, int Mol1, int Mol2, int Cd1, int Cd2>
		Prefixed_value<E, SI_unit<M1-M2, Kg1-Kg2, S1-S2, A1-A2, K1-K2, Mol1-Mol2, Cd1-Cd2>>
			operator/(const Prefixed_value<E, SI_unit<M1,Kg1,S1,A1,K1,Mol1,Cd1>>& lhs,
					  const Value<SI_unit<M2,Kg2,S2,A2,K2,Mol2,Cd2>>& rhs)
		{
			return Prefixed_value<E, SI_unit<M1-M2, Kg1-Kg2, S1-S2, A1-A2, K1-K2, Mol1-Mol2, Cd1-Cd2>>(lhs.val/rhs.val);
		}

		template<int E, int M1, int M2, int Kg1, int Kg2, int S1, int S2, int A1, int A2, int K1, int K2, int Mol1, int Mol2, int Cd1, int Cd2>
		Prefixed_value<-E, SI_unit<M1-M2, Kg1-Kg2, S1-S2, A1-A2, K1-K2, Mol1-Mol2, Cd1-Cd2>>
			operator/(const Value<SI_unit<M2,Kg2,S2,A2,K2,Mol2,Cd2>>& lhs,
					  const Prefixed_value<E, SI_unit<M1,Kg1,S1,A1,K1,Mol1,Cd1>>& rhs)
		{
			return Prefixed_value<-E, SI_unit<M1-M2, Kg1-Kg2, S1-S2, A1-A2, K1-K2, Mol1-Mol2, Cd1-Cd2>>(lhs.val/rhs.val);
		}

	}


	namespace Prefixes_example {
		using namespace Prefixes;
		using namespace Base;

		PREFIXED_VALUE(Volume_liters, -3, Volume, l);	// 1/1000 m3
		PREFIXED_VALUE(Area_hectars, 4, Area, ha);		// 10000 m2
		PREFIXED_VALUE(Length_km, 3, Length, km);		// 1000 m

	}

}


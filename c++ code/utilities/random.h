
#ifndef RANDOM_H
#define RANDOM_H

namespace Random {

	extern float next_float();	// [0,1]
	extern float next_float(float max); // [0,max]
	extern float next_float(float min, float max); // [min,max]
	extern int next_int(); // [0,INT_MAX)
	extern int next_int(int max); // [0,max)
	extern int next_int(int min, int max); // [0,max)

}

#endif
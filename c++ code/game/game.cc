#include <iostream>
#include "../items/include_all.h"
#include <vector>
#include "inventory.h"
#include "../crafting/templates.h"
#include "actions.h"
#include "object.h"
#include <map>

using namespace Items;
using namespace Crafting;

using namespace Game;




// // wrapper for a vector of item pointers
// // pointers are needed for polymorphism
// // all pointers are deleted when the inventory is destroyed
// class Inventory {
// public:
// 	void add(owner<Item*> item) { items_.push_back(item); }
// 	~Inventory()
// 	{
// 		for (auto item : items_)
// 			delete item;
// 	}

// private:
// 	std::vector<owner<Item*>> items_{};

// };


struct Character {
    Inventory inventory{};
    Equipped_items equipped_items{};
};

// global storage: all possible kinds of different items has its own container
// all items should be stored here so they can be referenced to, without fear of dangling pointers
static std::vector<Equipment> equipments{};
static std::vector<Weapon> weapons{};

static Action_handler action_handler{};
static std::map<std::string,Object*> objects{};    // Object = stuff you can do stuff with
static Character player{};

// returns nullptr if the object is not available
// prepositions:
//  time: on, in, at, since, for, ago, before, to, past, to, till, until, by
//  place: in, at, on, by, beside, under, below, over, above, across, through, to, into, towards, onto, from
//  other: from, of, by, on, in, off, by, at, about
Object* get_object(const std::string& key)
{
    Object* op = objects[key];
    if (op == nullptr) std::cout << "You can't find " << key << " here.\n";
    return op;
}

std::string get_key(std::istream& is, std::vector<std::string> allowed_prepositions = {}, int max_preps = 1)
{
    std::string key;
    is >> key;
    for (int i = 0; i < max_preps; i++) {
        for (auto prep : allowed_prepositions) {
            if (key == prep) {
                is >> key;
                break;
            }
        }
        if (is.eof())
            break;
    }
    return key;
}

Object* get_object(std::istream& is, std::vector<std::string> allowed_prepositions = {}, int max_preps = 1)
{
    return get_object(get_key(is, allowed_prepositions, max_preps));
}



// #define BASIC_ACTION(action, ...)                   \
// void action(std::istream& is)                       \
// {                                                   \
//     Object* object = get_object(is, {__VA_ARGS__}); \
//     if (object != nullptr)                          \
//         object->action();                           \
// }



void look(std::istream& is)
{
    Object* object = get_object(is, {"at", "in"});
    if (object != nullptr)
        object->examine();
}

void examine(std::istream& is)
{
    std::string key;
    is >> key;
    if (key == "")
        std::cout << "nothing to see here\n";
    else {
        Object* object = get_object(key);
        if (object != nullptr)
            object->examine();
    }
}

void hit(std::istream& is)
{
    Object* object = get_object(is);
    if (object != nullptr) {
        if (Weapon* wep = dynamic_cast<Weapon*>(player.equipped_items.get_equipped_slot(main_hand))) {
            if (wep == nullptr)
                std::cout << "no weapon" << std::endl;
                // todo: hit with default weapon (fist)
            else
                object->hit(*wep);
        }
    }
}

void take(std::istream& is)
{
    Object* object = get_object(is);
    if (object != nullptr) {
        object->take();
        if (Item* item = dynamic_cast<Item*>(object)) {
            player.inventory.add_item(item);
        }
    }
}







void eitest()
{
    Equipped_items& ei = player.equipped_items;

    Equipment e1{Identifier::leather_gloves};
    Equipment e2{Identifier::leather_legs};
    Equipment e2_copy{Identifier::leather_legs};
    Equipment e3{Identifier::cloth_chest};
    Equipment e4{Identifier::plate_chest};
    Weapon w1{Identifier::assault_spear_1h};
    Weapon w2{Identifier::assault_spear_1h};
    ei.equip(&e1);
    ei.equip(&e2);
    ei.equip(&e3);
    ei.equip(&e4);
    ei.equip(&w1);
    ei.equip(&w2);
    std::cout << ei.get_total_weight() << std::endl;
    std::cout << ei << std::endl << std::endl << std::endl;
    ei.unequip(&e1);
    ei.unequip_slot(Equip_slot::chest);
    ei.unequip(&e2_copy);
    ei.unequip_slot(Equip_slot::off_hand);
    ei.unequip(&w2);
    std::cout << ei.get_total_weight() << std::endl;
    std::cout << ei << std::endl;
}

void invtest()
{
    Inventory& inventory = player.inventory;
    Equipment e{Identifier::leather_gloves};
    Base_material bm = get_material_template(Material_type::metal);
    Item_stack<float> mats{&bm,100};
    inventory.add_item(&e);
    inventory.add_stack(mats);
    std::cout << inventory << std::endl;
    std::cout << inventory.get_total_weight() << std::endl;
}

void actiontest()
{
    Inventory& inventory = player.inventory;

    Equipment e{Identifier::leather_gloves};
    Base_material bm = get_material_template(Material_type::metal);
    Item_stack<float> mats{&bm,100};
    inventory.add_item(&e);
    inventory.add_stack(mats);
    std::cout << inventory << std::endl << std::endl;

    objects["inventory"] = &inventory;
    action_handler.add_action("look", look);
    action_handler.add_action("examine", examine);
    action_handler.add_action("hit", hit);
    action_handler.add_action("take", take);
    std::cin >> action_handler;
}

void keytest()
{
    std::stringstream ss{"a to in da"};
    std::cout << get_key(ss) << std::endl;
    ss = std::stringstream("a to in da");
    std::cout << get_key(ss, {"to", "in", "asdhk"},10) << std::endl;
    ss = std::stringstream("a to in da");
    std::cout << get_key(ss, {"a", "to", "da"},10) << std::endl;
}

int main() {
    // keytest();
    eitest();
	return 0;
}
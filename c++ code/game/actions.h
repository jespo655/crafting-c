#ifndef ACTIONS_H
#define ACTIONS_H

#include <map>	// class member

namespace Game {

	typedef void (*Action)(std::istream& subject);
	class string;
	class istream;


	class Action_handler {

	public:
		void add_action(const std::string& key, const Action& action);
		void perform_action(const std::string& key, std::istream& subject);

	private:
		std::map<std::string,Action> actions_{};

	};


	// input: read one word and perform the corresponding action
	// the action itself might read more words after that.
	std::istream& operator>>(std::istream&, Action_handler& ah);




} // namespace Game



#endif
#include "inventory.h"
#include "../items/include_all.h"
#include <iostream>
#include "../utilities/si_units.h"
#include <stdexcept>
#include "../items/equipment.h"
#include "../items/weapon.h"
#include "../items/secondary_stats.h"
#include <functional>               // lambda function


namespace Game {


    using namespace Items;

    // add_item assumes that the item references are taken care of elsewhere
    // this might lead to dangling pointers.

    bool Inventory::add_stack(Item_stack<int> is)
    {
        return is.add_to_list(integral_stacks_);
    }
    bool Inventory::add_stack(Item_stack<float> is)
    {
        return is.add_to_list(floating_point_stacks_);
    }

    bool Inventory::add_item(Item* item)
    {
        if (Stackable_item<int>* s_item = dynamic_cast<Stackable_item<int>*>(item))
            return add_stack(Item_stack<int>(s_item));

        if (Stackable_item<float>* s_item = dynamic_cast<Stackable_item<float>*>(item))
            return add_stack(Item_stack<float>(s_item));

        if (Item_stack<int>* stack = dynamic_cast<Item_stack<int>*>(item))
            return add_stack(*stack);

        if (Item_stack<float>* stack = dynamic_cast<Item_stack<float>*>(item))
            return add_stack(*stack);

        for (Item* i : items_)
            if (*i == *item)
                return false;

        items_.push_back(item);
        return true;
    }



    SI::Base::Mass Inventory::get_total_weight() const
    {
        SI::Base::Mass m{};
        for (auto a : items_)
            m+= a->get_weight();
        for (auto a : integral_stacks_)
            m += a.get_weight();
        for (auto a : floating_point_stacks_)
            m += a.get_weight();
        return m;
    }




    const Item* Inventory::get_item_at(int i) const
    {
        if (i < items_.size())
            return items_[i];
        if (i < items_.size()+integral_stacks_.size())
            return &integral_stacks_[i-items_.size()];
        else
            return &floating_point_stacks_[i-items_.size()-integral_stacks_.size()];
    }


    void Inventory::collapse()
    {
        std::vector<Item_stack<int>> int_stacks{};
        std::vector<Item_stack<float>> float_stacks{};

        for (auto a : integral_stacks_)
            a.add_to_list(int_stacks);

        for (auto a : floating_point_stacks_)
            a.add_to_list(float_stacks);

        integral_stacks_ = int_stacks;
        floating_point_stacks_ = float_stacks;
    }


    bool Inventory::examine() const
    {
        std::cout << *this;
        return true;
    }



    std::ostream& operator<< (std::ostream& os, const Inventory& inv) {
        if (inv.size() == 0)
            os << "The inventory is empty.\n";
        else
            for (int i=0; i < inv.size(); i++)
                os << (i+1) << ". " << *inv[i] << "\n";
        return os;
    }





    // returns a vector of all possible configurations of required slots
    std::vector<std::vector<Equip_slot>> required_slots(Equipment_type type)
    {
        switch(type) {
            case Equipment_type::head: return {{Equip_slot::head}};
            case Equipment_type::chest: return {{Equip_slot::chest}};
            case Equipment_type::full_body: return {{Equip_slot::chest, Equip_slot::legs}};
            case Equipment_type::hands: return {{Equip_slot::hands}};
            case Equipment_type::feet: return {{Equip_slot::feet}};
            case Equipment_type::legs: return {{Equip_slot::legs}};
            case Equipment_type::belt: return {{Equip_slot::belt}};
            case Equipment_type::small_shield: return {{Equip_slot::off_hand}};
            case Equipment_type::large_shield: return {{Equip_slot::off_hand}};
            case Equipment_type::one_handed_weapon: return {{Equip_slot::main_hand},{Equip_slot::off_hand}};
            case Equipment_type::two_handed_weapon: return {{Equip_slot::main_hand, Equip_slot::off_hand}};
            case Equipment_type::amulet: return {{Equip_slot::amulet}};
            case Equipment_type::ring: return {{Equip_slot::ring1}, {Equip_slot::ring2}};
        }
        return {};
    }




    bool Equipped_items::equip(Equipment* eq)
    {
        if (eq == nullptr || eq->identifier == Identifier::unknown) return false;         // cannot equip unknown items
        auto possible_reqs = required_slots(eq->equipment_type);
        for (std::vector<Equip_slot> reqs : possible_reqs) {
            // check if the slots are empty
            bool ok = true;
            for (Equip_slot slot : reqs) {
                if (equips_[slot]) {    // not nullptr
                    ok = false;
                    break;
                }
            }
            if (ok) {
                // equip the item
                for (auto slot : reqs) {
                    equips_[slot] = eq;
                }
                return true;
            }
        }
        // could not equip the item
        return false;
    }

    Equipment* Equipped_items::get_equipped_slot(Equip_slot slot) const
    {
        return equips_[slot];
    }

    bool Equipped_items::unequip(const Equipment* eq)
    {
        if (eq == nullptr || eq->identifier == Identifier::unknown) return false;
        bool unequipped = false;
        for (Equipment*& equipped : equips_) {
            if (equipped && *equipped == *eq) {
                equipped = nullptr;
                unequipped = true;
            }
        }
        return unequipped;
    }

    Equipment* Equipped_items::unequip_slot(Equip_slot slot)
    {
        Equipment* eq = get_equipped_slot(slot);
        unequip(eq);
        return eq;
    }




    void Equipped_items::for_each_item(std::function<void(const Equipment*)> f) const
    {
        for (int i = 0; i < equips_.size(); i++) {
            Equipment* eq = equips_[i];
            if (eq) {
                bool already_found = false;
                for (int j = 0; j < i; j++) {
                    if (equips_[i] == equips_[j]) {
                        already_found = true;
                        break;
                    }
                }
                if (!already_found)
                    f(eq);
            }
        }
    }


    SI::Base::Mass Equipped_items::get_total_weight() const
    {
        SI::Base::Mass tot{};
        for_each_item([&tot](const Equipment* eq){tot+=eq->get_weight();});
        return tot;
    }

    Secondary_stats Equipped_items::get_total_stats() const
    {
        Secondary_stats tot{};
        for_each_item([&tot](const Equipment* eq){tot+=eq->secondary_stats;});
        return tot;
    }





    std::string get_item_name(const Equipped_items& ei, Equip_slot slot)
    {
        Equipment* eq = ei.get_equipped_slot(slot);
        if (eq) return eq->name;
        return "";
    }

    std::ostream& operator<<(std::ostream& os, const Equipped_items& ei)
    {
        os << "head: " << get_item_name(ei,head) << "\n";
        os << "chest: " << get_item_name(ei,chest) << "\n";
        os << "legs: " << get_item_name(ei,legs) << "\n";
        os << "hands: " << get_item_name(ei,hands) << "\n";
        os << "feet: " << get_item_name(ei,feet) << "\n";
        os << "belt: " << get_item_name(ei,belt) << "\n";
        os << "amulet: " << get_item_name(ei,amulet) << "\n";
        os << "ring1: " << get_item_name(ei,ring1) << "\n";
        os << "ring2: " << get_item_name(ei,ring2) << "\n";

        os << "main_hand: " << get_item_name(ei,main_hand) << "\n";
        os << "off_hand: " << get_item_name(ei,off_hand);
        return os;
    }




} // namespace Game
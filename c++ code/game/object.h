#ifndef OBJECT_H
#define OBJECT_H

#include <iostream>

namespace Items {
    class Weapon;
}

namespace Game {

	// a object is something that you can do stuff to
	class Object {
	public:

		// each action should return true if the action succeeded
		virtual bool examine() const { std::cout << "You can't examine that!" << std::endl; return false; }
		virtual bool hit(const Items::Weapon& weapon) const { std::cout << "You can't hit that!" << std::endl; return false; }
		virtual bool take() const { std::cout << "You can't take that!" << std::endl; return false; }
		virtual bool use() const { std::cout << "You can't use that!" << std::endl; return false; }
		virtual bool talk_to() const { std::cout << "You can't talk to that!" << std::endl; return false; }

	};

} // namespace Game

#endif

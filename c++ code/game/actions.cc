#include "actions.h"

#include <iostream>
#include <string>
#include <sstream>

namespace Game {


	void Action_handler::add_action(const std::string& key, const Action& action)
	{
		actions_[key] = action;	// overwrites if the key already has an action
	}
	void Action_handler::perform_action(const std::string& key, std::istream& subject)
	{
		Action action = actions_[key];
		if (action == nullptr) std::cout << "You don't know how to do that!" << std::endl;
		else action(subject);
	}



	// input: read one word and perform the corresponding action
	// the action itself might read more words after that.
	std::istream& operator>>(std::istream& is, Action_handler& ah)
	{
		std::string key, subject;
		is >> key;
        std::getline(is,subject);
        std::stringstream ss{subject};
		ah.perform_action(key, ss);
		return is;
	}



} // namespace Game





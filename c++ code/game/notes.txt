

action phrase: [verb] [subject]



actions:

take item and put it in inventory (take, grab, loot)
put an item from inventory to the ground (put x on y, leave)
use available [usable] (use)
look around to get an description of stuff you can see
	(look, look at, explore, investigate)
equip item (item has to be in your inventory) (equip, don, put on)
go somewhere else (n,N,north,toward x)


crafting:
	use recipe
	choose what materials to use
	craft item



numbers are stashed for the next command
int n
array<string,n>

-> look inventory
Your inventory contains:
	1. Basic Leather (50)				// (n) = quantity
	2. Strong Leather (20)
	3. Heavy Leather (100)
	4. Red Gem (3)
	5. Leather Gloves Recipe (U)		// (U) = usable flag
-> use Leather Gloves Recipe
Materials needed:
	50 Leather
	Up to 2 Decoration Materials
Which materials to use?
	1. Basic Leather (50)
	2. Strong Leather (20)
	3. Heavy Leather (100)
	4. Red Gem (3)
-> 2 3 4
Item crafted: Leather Gloves!
-> look inventory
Your inventory contains:
	1. Basic Leather (50)
	2. Heavy Leather (70)
	3. Red Gem (1)
	4. Leather Gloves
-> don 4
You put on the Leather Gloves.








combat:
use weapons (hit, smash, bash, stab, slash, etc.)
use usables (use x, use x on y)
cast spell (cast, magic, spell)
run away (to a random nearby location, or run in circle to the same location)


magic: spell is a combination of 10 possible signs (0-9)
longer string -> more powerful spell
have to come up with the spell before it can be used (can't use random characters)


options:
	spells can be found in the world
	experiments can result in new spells


each spell has some properties that specify what it does
	element (fire, water, air, earth?)
	type (missile, trap, buff)
	mana cost
	cast time (proportional to string length)

spells can be combined (cast !&¤ #@*) to form a multi-element and/or multi-type spell
mana cost is sum of all mana costs
cast time is suum of all cast times + # spells - 1 (spaces between spells counts)

























§1234567890+´¨',.-
½!"#¤%&/()=?`^*;:_
@£$€{[]}\~€µ



1234567890
@#$%&*!¤£€


!"#%&'()*+´-./
:;<=>?
[\]^_`{|}-
£





inventory: a vector with item stacks
if you try to add something that is not a stack, create a new stack with size 1
item_stack has a reference to an item -> that reference has to be taken extra care of to
	avoid dangling pointers





(TODO:)
move constructor in item (copy constructor should get a new unique id)
	testa vilken som används var. Använd std::move()?

var är detta viktigt? vill vi ha ett nytt item så använder vi inte move constructor, utan tex. identifier constructor


TEST:
	A() { i = 2 }	// körs i++?
	i{id++}

	init till -1, sen sätt till unq om item.id inte är unknown





TODO:
cin.getline(cmd) för Commands - hela stringen bör användas för command
Om stringen inte innehåller tillräckligt många ord ska ett felmeddelande skickas

TODO:
Gå tillbaka så att stackable_item är en subklass av item
Inventory har 3 listor: Item_stack<int>, Item_stack<float>, Item*
Om Item* som försöker läggas in i listan är en subklass av Stackable_item<N> skapas en itemstack med stack size 1 istället.



TODO:
kolla lambda functions i c++
[](auto x, auto y) { return x-y; }


void func3(std::vector<int>& v) {
	std::for_each(v.begin(), v.end(), [](int) { /* do something here*/ });
}


using returnvalue = double;
[](double d) -> returnvalue { return d; }

[]: capture list - gör att man kan skicka med lokala variabler till en lambda som kan användas någon helt annanstans
[=]: alla variabler i scope (by value)
[&]: alla variabler i scope (by reference)
[a,&b]: specifikt a (by value) och b (by reference)
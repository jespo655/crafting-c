#ifndef INVENTORY_H
#define INVENTORY_H

#include "object.h"                 // parent class
#include <vector>                   // class member
#include <functional>               // lambda function
#include "../items/item_stack.h"    // template class

namespace Items {
    class Weapon;
    class Equipment;
    class Secondary_stats;
}


namespace Game {

    using namespace Items;

    class ostream;

    // The Inventory is a container of item stacks.
    // Each item stack has a pointer to a Stackable_item. That item
    //      has to be taken extra care of outside of this class.
    // TODO: refactor so that dangling pointers is not a possibility
    class Inventory : public Object {

    public:

        // add_item assumes that the item references are taken care of elsewhere
        // this might lead to dangling pointers.
        // returns false if the item was already in the inventory
        bool add_item(Item* item);
        bool add_stack(Item_stack<int> is);
        bool add_stack(Item_stack<float> is);

        template<typename Number>
        bool add_stack(Stackable_item<Number>* item, Number quantity = 1) { return add_stack(Item_stack<Number>(item,quantity)); }

        int size() const { return integral_stacks_.size() + floating_point_stacks_.size(); }
        SI::Base::Mass get_total_weight() const;

        const Item* get_item_at(int i) const;
        const Item* operator[](int i) const { return get_item_at(i); }

        void collapse();

        bool examine() const override;


    private:
        std::vector<Item*> items_{};
        std::vector<Item_stack<int>> integral_stacks_{};
        std::vector<Item_stack<float>> floating_point_stacks_{};
    };

    std::ostream& operator<< (std::ostream& os, const Inventory& inv);

    // position_cache_.clear();
    // position_cache_.push_back(i);
    // std::vector<int> position_cache_{};





    enum Equip_slot {
        // equipment slots:
        head, chest, legs, hands, feet, belt,
        amulet, ring1, ring2,

        // weapon slots
        main_hand, off_hand,

        eq_slot_MIN = head,
        eq_slot_MAX = ring2,
        wep_slot_MIN = main_hand,
        wep_slot_MAX = off_hand,
        slot_MAX = off_hand
    };


    class Equipped_items {
    public:

        bool equip(Equipment* eq);

        Equipment* get_equipped_slot(Equip_slot slot) const;

        bool unequip(const Equipment* eq);
        Equipment* unequip_slot(Equip_slot slot);

        SI::Base::Mass get_total_weight() const;
        Secondary_stats get_total_stats() const;

        // TODO:
        // get total weight
        // get total stats
        // get avg flexibility (with slot coefficients?)

    private:
        std::array<Equipment*,slot_MAX+1> equips_{};

        void for_each_item(std::function<void(const Equipment*)>) const;

    };


    std::ostream& operator<<(std::ostream& os, const Equipped_items& ei);

}

#endif
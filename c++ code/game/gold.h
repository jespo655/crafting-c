// stackable_item.h
#ifndef GOLD_H
#define GOLD_H

#include "item.h"

#include <iostream>

namespace Items {

	// N should be a number type

	// template<typename N>
	// class Stackable_item : public virtual Item {

	// public:
	// 	virtual N get_max_stack_size() const { return 9999; }

	// };


	class Gold : public Stackable_item<int> {

	public:
		Gold() : Item(Identifier::gold) {}
		int get_max_stack_size() const override { return 9999; }

	};




} // namespace Items

#endif
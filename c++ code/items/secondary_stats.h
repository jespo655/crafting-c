// secondary_stats.h
#ifndef SECONDARY_STATS_H
#define SECONDARY_STATS_H

#include <iostream>
#include <iomanip>
#include <array>
#include "../utilities/random.h"

namespace Items {

	enum Primary_stat {
		level,
		strength,
		dexterity,
		intelligence,

		primary_stat_MAX = intelligence
	};

	enum Secondary_stat {
		// str-based stats
		health_regen,

		// dex-based stats
		movement_speed,

		// int-based stats
		mana_regen,
		elemental_resistance,

		// used for lists
		secondary_stat_MAX = elemental_resistance
	};


	class Primary_stats : public std::array<int,primary_stat_MAX+1> {
	public:
		Primary_stats(const std::array<int,primary_stat_MAX+1>& arr) : std::array<int,primary_stat_MAX+1>(arr) {}
		Primary_stats() : std::array<int,primary_stat_MAX+1>() {}

		bool operator<(const Primary_stats& rhs) {
			for (int i = 0; i <= primary_stat_MAX; i++)
				if ((*this)[i] >= rhs[i])
					return false;
			return true;
		}

		bool operator>(const Primary_stats& rhs) {
			for (int i = 0; i <= primary_stat_MAX; i++)
				if ((*this)[i] <= rhs[i])
					return false;
			return true;
		}

		bool operator==(const Primary_stats& rhs) {
			for (int i = 0; i <= primary_stat_MAX; i++)
				if ((*this)[i] != rhs[i])
					return false;
			return true;
		}

		bool operator>=(const Primary_stats& rhs) { return !(*this < rhs); }
		bool operator<=(const Primary_stats& rhs) { return !(*this > rhs); }
		bool operator!=(const Primary_stats& rhs) { return !(*this == rhs); }

		// int& operator[](int i) { return std::array<int,primary_stat_MAX+1>[i]; }

	};


	inline std::ostream& operator<<(std::ostream& os, const Primary_stats ps) {
		os << "strength: " << ps[strength]
			<< ", dexterity: " << ps[dexterity]
			<< ", intelligence: " << ps[intelligence] << "\n";
		return os;
	}


	class Secondary_stats : public std::array<float,secondary_stat_MAX+1> {
	public:
		Secondary_stats() : std::array<float,secondary_stat_MAX+1>{} {}
		Secondary_stats(const float f) : std::array<float,secondary_stat_MAX+1>{} { (*this) += f; }
		Secondary_stats(const std::array<float,secondary_stat_MAX+1>& arr) : std::array<float,secondary_stat_MAX+1>(arr) {}

		// float& operator[](int i) { return std::array<float,secondary_stat_MAX+1>[i]; }

		// modify with other Secondary_stats
		Secondary_stats& operator+=(const Secondary_stats& rhs) {
			for (int i = 0; i <= secondary_stat_MAX; i++) {
				(*this)[i] += rhs[i];
			}
			return *this;
		}

		Secondary_stats& operator-=(const Secondary_stats& rhs) {
			for (int i = 0; i <= secondary_stat_MAX; i++) {
				(*this)[i] -= rhs[i];
			}
			return *this;
		}

		Secondary_stats& operator*=(const Secondary_stats& rhs) {
			for (int i = 0; i <= secondary_stat_MAX; i++) {
				(*this)[i] *= rhs[i];
			}
			return *this;
		}

		Secondary_stats& operator/=(const Secondary_stats& rhs) {
			for (int i = 0; i <= secondary_stat_MAX; i++) {
				(*this)[i] /= rhs[i];
			}
			return *this;
		}

		friend Secondary_stats operator+(const Secondary_stats& lhs, const Secondary_stats& rhs) {
			return Secondary_stats(lhs) += rhs;
		}

		friend Secondary_stats operator-(const Secondary_stats& lhs, const Secondary_stats& rhs) {
			return Secondary_stats(lhs) -= rhs;
		}

		friend Secondary_stats operator*(const Secondary_stats& lhs, const Secondary_stats& rhs) {
			return Secondary_stats(lhs) *= rhs;
		}

		friend Secondary_stats operator/(const Secondary_stats& lhs, const Secondary_stats& rhs) {
			return Secondary_stats(lhs) /= rhs;
		}



		// modify with number
		Secondary_stats& operator+=(const float& rhs) {
			for (int i = 0; i <= secondary_stat_MAX; i++) {
				(*this)[i] += rhs;
			}
			return *this;
		}

		Secondary_stats& operator-=(const float& rhs) {
			for (int i = 0; i <= secondary_stat_MAX; i++) {
				(*this)[i] -= rhs;
			}
			return *this;
		}

		Secondary_stats& operator*=(const float& rhs) {
			for (int i = 0; i <= secondary_stat_MAX; i++) {
				(*this)[i] *= rhs;
			}
			return *this;
		}

		Secondary_stats& operator/=(const float& rhs) {
			for (int i = 0; i <= secondary_stat_MAX; i++) {
				(*this)[i] /= rhs;
			}
			return *this;
		}

		friend Secondary_stats operator+(const Secondary_stats& lhs, const float& rhs) {
			return Secondary_stats(lhs) += rhs;
		}

		friend Secondary_stats operator-(const Secondary_stats& lhs, const float& rhs) {
			return Secondary_stats(lhs) -= rhs;
		}

		friend Secondary_stats operator*(const Secondary_stats& lhs, const float& rhs) {
			return Secondary_stats(lhs) *= rhs;
		}

		friend Secondary_stats operator/(const Secondary_stats& lhs, const float& rhs) {
			return Secondary_stats(lhs) /= rhs;
		}

	};


	inline std::ostream& operator<<(std::ostream& os, const Secondary_stats ss)
	{
		os << std::fixed << std::setprecision(1);

		os << "life regen: " << ss[health_regen]
			<< ", movement speed: " << ss[movement_speed] << "\n";

		os << "mana regen: " << ss[mana_regen]
			<< ", elemental resist: " << ss[elemental_resistance] << "\n";

		return os;
	}

	inline Secondary_stats random_between(const Secondary_stats& min, const Secondary_stats& max)
	{
		Secondary_stats ss{min};
		for (int i = 0; i <= secondary_stat_MAX; i++)
			ss[i] += (max[i]-min[i]) * Random::next_float();
		return ss;
	}

	inline Secondary_stats random_n_with_total(int n, float total)
	{
		// choose n stats randomly
		// randomize them between 0 and 1
		// multiply with something so the total is reached
		std::array<bool,secondary_stat_MAX+1> chosen_stats{};

		// chose n (algorithm found at SO: http://stackoverflow.com/a/2394292)
		for (int i = secondary_stat_MAX+1 - n; i < secondary_stat_MAX+1; i++) {
			int pos = Random::next_int(i);
			if (!chosen_stats[pos])
				chosen_stats[pos] = true;
			else
				chosen_stats[i] = true;
		}

		// assign random stats
		Secondary_stats ss{};
		float total_stats{0};
		for (int i = 0; i <= secondary_stat_MAX; i++) {
			if (chosen_stats[i]) {
				ss[i] = Random::next_float();
				total_stats += ss[i];
			}
		}

		// scale them correctly
		if (total_stats != 0)
			ss *= (total / total_stats);
		return ss;
	}

	// works as random_n_with_total() but with random number of random stats
	inline Secondary_stats random_stats_with_total(float total)
	{
		int n = Random::next_int(secondary_stat_MAX+1);
		return random_n_with_total(n,total);
	}
} // namespace Items

#endif
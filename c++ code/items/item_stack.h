// item_stack.h
#ifndef ITEM_STACK_H
#define ITEM_STACK_H

#include "item.h"						// parent class
#include "crafting_material.h"			// parent class
#include <vector>
#include <string>
#include <iostream>

namespace Items {

    class Mass;

	// Number = int, float
	template<typename Number = float>
	class Item_stack : public Item, public Crafting_material {


	public:
		Item_stack(const Stackable_item<Number>* s_item, Number quantity = 1)
			: Item(s_item->identifier, s_item->name), item_{s_item}, quantity_{quantity}
		{
			if (const Crafting_material* cmp = dynamic_cast<const Crafting_material*>(s_item)) {
				secondary_stats = cmp->secondary_stats;
				material_type = cmp->material_type;
				level = cmp->level;
			}
            unit_weight = s_item->unit_weight;
		}


		// Quantity methods
		float get_float_quantity() const override { return static_cast<float>(get_quantity()); }
		Number get_quantity() const { return quantity_; }
		bool is_empty() const { return quantity_ <= 0; }
		std::string get_quantity_string() const;

		bool is_integral_type() const;
        Number get_max_stack_size() const { std::cout << "is gmss used\n"; return item_->get_max_stack_size(); }

		// Crafting methods
		bool use() override { return use(quantity_); }		// use everything
		bool use(Number amount);
		bool is_used() const override { return is_empty(); }

        SI::Base::Mass get_weight() const override;
		SI::Base::Mass get_unit_weight() const override { return unit_weight; }
		const std::string get_flags() const override { return item_->get_flags(); }
		const std::string get_meta_data() const override;

		// Stack size modification

		// move as many items as possible from another stack to this one.
		// return true if any items was moved.
		bool combine(Item_stack& is);

		// combines this stack to a list that could contain other item stacks
		// returns true if anything was added to the list
		// returns false if this stack was already in the list
		bool add_to_list(std::vector<Item*>& list);
		bool add_to_list(std::vector<Crafting_material*>& list);
		bool add_to_list(std::vector<Item_stack>& list);


		// move the specified amount of items to a new stack
		// if amount >= size, then move all items.
		Item_stack<Number> split(const Number amount);

		// like split, but does not subtract the amount from this stack
		Item_stack<Number> clone(const Number amount) const;

		const Stackable_item<Number>* get_reference() const { return item_; }

	private:
		const Stackable_item<Number>* item_;		// used as a reference, but needs to be a pointer to allow overwriting it with a new reference (default = assignment)
		Number quantity_;


	};



} // namespace Items

#endif
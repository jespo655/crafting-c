// crafting_material.h
#ifndef CRAFTING_MATERIAL_H
#define CRAFTING_MATERIAL_H

#include "../external/string_enum.h"
#include "secondary_stats.h"			// class member
#include <string>

// CraftingMaterial is anything that can be used when crafting other items.
// This can even include other crafted items.

// Only items that derives from this class can be attempted to be crafted from.


namespace Items {

    ENUM(Material_type, int,
        metal, leather, cloth, wood, decoration, other,
        material_MAX = other,
        base_material_MAX = wood
    );

    class Mass;

	class Crafting_material {

	public:
		Crafting_material() {}

		Crafting_material(Material_type type) : material_type{type} {}

		Crafting_material(const Secondary_stats& ss, Material_type type, int lvl)
            : secondary_stats{Secondary_stats(ss)},
				material_type{type},
				level{lvl}
				{}

		Crafting_material(const Crafting_material& cm) = default;
        virtual ~Crafting_material() = default;

		virtual bool use() { // return true if successfully used the item
			bool success = !used_;
			used_ = true;
			return success;
		};
		virtual bool is_used() const { return used_; };

		// virtual getters
		virtual float get_float_quantity() const { return 1; }
        virtual SI::Base::Mass get_unit_weight() const = 0;

		Secondary_stats secondary_stats{};
		Material_type material_type{Material_type::other};
		int level{0};

		virtual const std::string get_flags() const { return "C"; }

	private:
		bool used_ = false;

	};

} // namespace Items

#endif
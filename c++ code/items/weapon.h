#ifndef WEAPON_H
#define WEAPON_H

#include <string>
#include "equipment.h"		// parent class
#include "item.h"			// enums Weapon_type, Identifier

namespace Items {

	using SI::Base::Frequency;
	using Damage = SI::Value<SI::Base::Dimensionless>;

	struct Weapon : public Equipment {

		Weapon() {}
		Weapon(std::string name) : Equipment(name) {}
		Weapon(Identifier id);

		bool is_two_handed() const { return Items::is_two_handed(weapon_type); }
		bool is_ranged() const { return Items::is_ranged(weapon_type); }

		Damage damage{0};				// damage per hit
		Frequency attack_speed{0};		// attacks / second

		Weapon_type weapon_type{Weapon_type::none};
		// SI::Base::Length reach{0}; 			// how far you can reach with melee attach
		// SI::Base::Length range{0};			// how far you can throw or shoot with the weapon

	};

	std::ostream& operator<<(std::ostream& os, const Weapon& e);

} // namespace Items

#endif
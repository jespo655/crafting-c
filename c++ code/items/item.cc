#include "item.h"


namespace Items {

    Equipment_type get_equipment_type(Identifier id) {
    	switch(id) {

	        case Identifier::leather_helmet:
	        case Identifier::plate_helmet:
	        case Identifier::chainmail_helmet:
	        case Identifier::cloth_helmet:
	        case Identifier::wooden_helmet:
            	return Equipment_type::head;

	        case Identifier::leather_chest:
	        case Identifier::plate_chest:
	        case Identifier::chainmail_chest:
	        case Identifier::cloth_chest:
	        case Identifier::wooden_chest:
            	return Equipment_type::chest;

			case Identifier::leather_full_body:
			case Identifier::plate_full_body:
			case Identifier::chainmail_full_body:
			case Identifier::cloth_full_body:
			case Identifier::wooden_full_body:
	        	return Equipment_type::full_body;

	        case Identifier::leather_gloves:
	        case Identifier::plate_gloves:
	        case Identifier::chainmail_gloves:
	        case Identifier::cloth_gloves:
	        case Identifier::wooden_gloves:
            	return Equipment_type::hands;

	        case Identifier::leather_boots:
	        case Identifier::plate_boots:
	        case Identifier::chainmail_boots:
	        case Identifier::cloth_boots:
	        case Identifier::wooden_boots:
	        	return Equipment_type::feet;

	        case Identifier::leather_legs:
	        case Identifier::plate_legs:
	        case Identifier::chainmail_legs:
	        case Identifier::cloth_legs:
	        case Identifier::wooden_legs:
            	return Equipment_type::legs;

	        case Identifier::leather_belt:
	        case Identifier::plate_belt:
	        case Identifier::chainmail_belt:
	        case Identifier::cloth_belt:
	        case Identifier::wooden_belt:
	        	return Equipment_type::belt;

	        case Identifier::leather_small_shield:
	        case Identifier::plate_small_shield:
	        case Identifier::chainmail_small_shield:
	        case Identifier::cloth_small_shield:
	        case Identifier::wooden_small_shield:
	        	return Equipment_type::small_shield;

	        case Identifier::leather_large_shield:
	        case Identifier::plate_large_shield:
	        case Identifier::chainmail_large_shield:
	        case Identifier::cloth_large_shield:
	        case Identifier::wooden_large_shield:
	        	return Equipment_type::large_shield;

	    	case Identifier::amulet:
	    		return Equipment_type::amulet;

			case Identifier::ring:
				return Equipment_type::ring;

	        case Identifier::dagger:
	        case Identifier::sword_1h:
	        case Identifier::battle_axe_1h:
			case Identifier::assault_spear_1h:
			case Identifier::warhammer_1h:
	        case Identifier::fist_weapon:
	        	return Equipment_type::one_handed_weapon;

	        case Identifier::sword_2h:
	        case Identifier::battle_axe_2h:
	        case Identifier::assault_spear_2h:
	        case Identifier::polearm:
	        case Identifier::warhammer_2h:
	        case Identifier::bow:
	        case Identifier::staff:
	        	return Equipment_type::two_handed_weapon;

        	default:
    			return Equipment_type::none;
    	}
    }

    Equipment_material get_equipment_material(Identifier id) {
        switch (id) {
            case Identifier::leather_helmet:
            case Identifier::leather_chest:
            case Identifier::leather_full_body:
            case Identifier::leather_gloves:
            case Identifier::leather_boots:
            case Identifier::leather_legs:
            case Identifier::leather_belt:
            case Identifier::leather_small_shield:
            case Identifier::leather_large_shield:
                return Equipment_material::leather;

            case Identifier::plate_helmet:
            case Identifier::plate_chest:
            case Identifier::plate_full_body:
            case Identifier::plate_gloves:
            case Identifier::plate_boots:
            case Identifier::plate_legs:
            case Identifier::plate_belt:
            case Identifier::plate_small_shield:
            case Identifier::plate_large_shield:
                return Equipment_material::plate;

            case Identifier::chainmail_helmet:
            case Identifier::chainmail_chest:
            case Identifier::chainmail_full_body:
            case Identifier::chainmail_gloves:
            case Identifier::chainmail_boots:
            case Identifier::chainmail_legs:
            case Identifier::chainmail_belt:
            case Identifier::chainmail_small_shield:
            case Identifier::chainmail_large_shield:
                return Equipment_material::chainmail;

            case Identifier::cloth_helmet:
            case Identifier::cloth_chest:
            case Identifier::cloth_full_body:
            case Identifier::cloth_gloves:
            case Identifier::cloth_boots:
            case Identifier::cloth_legs:
            case Identifier::cloth_belt:
            case Identifier::cloth_small_shield:
            case Identifier::cloth_large_shield:
                return Equipment_material::cloth;

            case Identifier::wooden_helmet:
            case Identifier::wooden_chest:
            case Identifier::wooden_full_body:
            case Identifier::wooden_gloves:
            case Identifier::wooden_boots:
            case Identifier::wooden_legs:
            case Identifier::wooden_belt:
            case Identifier::wooden_small_shield:
            case Identifier::wooden_large_shield:
                return Equipment_material::wood;

            case Identifier::amulet:
            case Identifier::ring:
                return Equipment_material::jewellery;

            default:
                return Equipment_material::other;
        }
    }

    Weapon_type get_weapon_type(Identifier id) {
        switch (id) {
            case Identifier::dagger: return Weapon_type::dagger;
            case Identifier::sword_1h: return Weapon_type::sword_1h;
            case Identifier::sword_2h: return Weapon_type::sword_2h;
            case Identifier::battle_axe_1h: return Weapon_type::battle_axe_1h;
            case Identifier::battle_axe_2h: return Weapon_type::battle_axe_2h;
            case Identifier::assault_spear_1h: return Weapon_type::assault_spear_1h;
            case Identifier::assault_spear_2h: return Weapon_type::assault_spear_2h;
            case Identifier::polearm: return Weapon_type::polearm;
            case Identifier::warhammer_1h: return Weapon_type::warhammer_1h;
            case Identifier::warhammer_2h: return Weapon_type::warhammer_2h;
            case Identifier::fist_weapon: return Weapon_type::fist_weapon;
            case Identifier::bow: return Weapon_type::bow;
            case Identifier::staff: return Weapon_type::staff;
            default: return Weapon_type::none;
        }
    }

    bool is_weapon(Identifier i) { return get_weapon_type(i) != Weapon_type::none; }
    bool is_equipment(Identifier i) { return get_equipment_type(i) != Equipment_type::none; }

    bool is_ranged(Weapon_type w) { return w == Weapon_type::bow; }
    bool is_two_handed(Weapon_type w) {
    	switch (w) {
	        case Weapon_type::sword_2h:
	        case Weapon_type::battle_axe_2h:
	        case Weapon_type::assault_spear_2h:
	        case Weapon_type::polearm:
	        case Weapon_type::warhammer_2h:
	        case Weapon_type::bow:
	        case Weapon_type::staff:
	        	return true;
    	}
    	return false;
    }




	std::ostream& operator<<(std::ostream& os, const Identifier& id) {
	    os << id._to_string();
	    return os;
	}

	std::ostream& operator<<(std::ostream& os, const Equipment_type& id) {
	    os << id._to_string();
	    return os;
	}

	std::ostream& operator<<(std::ostream& os, const Equipment_material& id) {
	    os << id._to_string();
	    return os;
	}

	std::ostream& operator<<(std::ostream& os, const Weapon_type& id) {
	    os << id._to_string();
	    return os;
	}

    std::ostream& operator<<(std::ostream& os, const Item& i) {
        os << i.name;
        std::string meta_data = i.get_meta_data();
        if (meta_data != "")
            os << " " << meta_data;
        return os;
    }


} // namespace Items






#include "item_stack.h"
#include <iostream>		// print quantity
#include <iomanip>		// print quantity
#include "../utilities/si_units.h"		// SI::Base::Weight
#include <algorithm>	// std::find
#include <string>
#include <sstream>


namespace Items {

	// This is a list of all legal specializations of Item_stack<N>
	// Extending this list will probably break something
	// (for example crafting/crafting_table.cc@Available_materials::craft_equipment)
	// 		Be careful!
	template class Item_stack<int>;
	template class Item_stack<float>;


	template<> bool Item_stack<int>::is_integral_type() const { return true; }
	template<> bool Item_stack<float>::is_integral_type() const { return false; }




	template<typename N>
	std::string Item_stack<N>::get_quantity_string() const
	{
		if (quantity_ == 1) return "";
		std::stringstream ss{};
		ss << std::fixed << std::setprecision(2) << "(" << quantity_ << ")";
		return ss.str();
	}


	// Crafting methods
	template<typename N>
	bool Item_stack<N>::use(N amount)
	{
		if (amount < 0 || amount > quantity_) return false;
		quantity_ -= amount;
		return true;
	}



 	template<typename N>
	SI::Base::Mass Item_stack<N>::get_weight() const
	{
		return unit_weight * SI::Base::Value<SI::Base::Dimensionless>(quantity_);
	}


	template<typename N>
	const std::string Item_stack<N>::get_meta_data() const
	{
		std::string meta_data = item_->get_meta_data();
		if (meta_data == "") return get_quantity_string();
		std::string quantity_string = get_quantity_string();
		if (quantity_string == "") return meta_data;
		return quantity_string + " " + meta_data;
	}



	// Stack size modification

	// move as many items as possible from another stack to this one.
	// return true if any items was moved.
	template<typename N>
	bool Item_stack<N>::combine(Item_stack<N>& is)
	{
		if (is == *this) return false;	// can't move from itself
		if (*(is.item_) != *item_) return false;	// can't combine different types of items

		N total_quantity = quantity_ + is.quantity_;
		N max_quantity = item_->get_max_stack_size();

		if (total_quantity > max_quantity) {
			quantity_ = max_quantity;
			is.quantity_ = total_quantity - max_quantity;
		} else {
			quantity_ = total_quantity;
			is.quantity_ = 0;
		}
		return true;
	}


	// adds this stack to a list that could contain other item stacks
	// returns true if anything was added to the list
	// returns false if this stack was already in the list
	template<typename N>
	bool Item_stack<N>::add_to_list(std::vector<Item*>& list)
	{
		if (is_empty()) return false;
        for (auto a : list) {
            if (*this == *a)
                return false;     // this was already in the list
        }
		for (auto a : list) {
			if (Item_stack<N>* is = dynamic_cast<Item_stack<N>*>(a)) {
				is->combine(*this);
				if (is_empty()) return true;
			}
		}
		list.push_back(this);
		return true;
	}

	template<typename N>
	bool Item_stack<N>::add_to_list(std::vector<Crafting_material*>& list)
	{
		if (is_empty()) return false;
		if (dynamic_cast<const Crafting_material*>(item_) == nullptr)	{
			// incompatible type
			return false;
		}
        for (auto a : list) {
        	if (Item* item = dynamic_cast<Item*>(a))
	            if (*this == *item)
	                return false;     // this was already in the list
        }
		for (auto a : list) {
			if (Item_stack<N>* is = dynamic_cast<Item_stack<N>*>(a)) {
				is->combine(*this);
				if (is_empty()) return true;
			}
		}
		list.push_back(this);
		return true;
	}


	template<typename N>
	bool Item_stack<N>::add_to_list(std::vector<Item_stack>& list)
	{
		if (is_empty()) return false;
        for (auto a : list) {
            if (*this == a)
                return false;     // this was already in the list
        }
		for (auto is : list) {
			is.combine(*this);
			if (is_empty()) return true;
		}
		list.push_back(*this);
		return true;
	}









	// move the specified amount of items to a new stack
	// if amout >= size, then move all items.
	template<typename N>
	Item_stack<N> Item_stack<N>::split(N amount)
	{
		Item_stack<N> is{item_, 0};
		if (amount <= 0) return is; // can't move negative items

		if (amount < quantity_) {
			// should not move everything -> something is left in this stack
			is.quantity_ = amount;
			quantity_ -= amount;
		} else {
			// move everything
			is.quantity_ = quantity_;
			quantity_ = 0;
		}
		return is;
	}


	// like split, but does not subtract the amount from this stack
	template<typename N>
	Item_stack<N> Item_stack<N>::clone(N amount) const {
		N max_quantity = item_->get_max_stack_size();
		if (amount <= max_quantity)
			return Item_stack<N>(item_, amount);
		else
			return Item_stack<N>(item_, max_quantity);
	}



} // namespace Items
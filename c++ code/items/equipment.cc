#include "equipment.h"
#include "../utilities/assert.h"


namespace Items {


	Equipment::Equipment(Identifier id)
		: Item(id),
			equipment_type{get_equipment_type(id)},
			material{get_equipment_material(id)}
	{
		// if not equipment identifier, fail.
		ASSERT(is_equipment(id), "Equipment must have equipment identifier!");
	}

	std::ostream& operator<<(std::ostream& os, const Equipment& e) {

		os << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

		// Level 20 leather helmet with name "leather helmet"
		os << "Level " << e.level << " " << e.material << " "
			<< e.equipment_type << " with name \"" << e.name << "\"\n";

		// Req: level 20, 10 str, 10 dex, 0 int
		os << "Req: level " << e.stat_requirements[level] << ", "
			<< e.stat_requirements[strength] << " str, "
			<< e.stat_requirements[dexterity] << " dex, "
			<< e.stat_requirements[intelligence] << " int\n";

		// Weight: 0.5, Armour: 5.0, Flexibility: 0.752
		os << std::fixed << std::setprecision(1);
		os << "Weight: " << e.get_weight() << ", Armour: " << e.armour;
		os << std::setprecision(3);
		os << ", Flexibility: " << e.flexibility << "\n";

		os << e.secondary_stats;

		os << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

		return os;
	}

}
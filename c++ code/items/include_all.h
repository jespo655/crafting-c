// items/include_all.h
#ifndef INCLUDE_ALL_ITEMS_H
#define INCLUDE_ALL_ITEMS_H

#include "base_material.h"
#include "crafting_material.h"
#include "equipment.h"
#include "item.h"
#include "item_stack.h"
#include "secondary_stats.h"
#include "weapon.h"

#endif
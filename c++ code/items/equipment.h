#ifndef EQUIPMENT_H
#define EQUIPMENT_H

#include <string>
#include <iostream>
#include "item.h"					// parent class
#include "crafting_material.h"		// parent class
#include "secondary_stats.h"		// class member

namespace Items {

    class Mass;

	struct Equipment : public Item, public Crafting_material {

		Equipment() {}
		Equipment(std::string name) : Item(Identifier::unknown, name) {}
		Equipment(Identifier id);

		float flexibility{0};	// [0,1]
		float armour{0};

		Equipment_type equipment_type{Equipment_type::none};
		Equipment_material material{Equipment_material::other};
		Primary_stats stat_requirements{};

		const std::string get_flags() const override { return Crafting_material::get_flags()+Item::get_flags()+"E"; }
        SI::Base::Mass get_unit_weight() const override { return unit_weight; }
	};

	std::ostream& operator<<(std::ostream& os, const Equipment& e);

}

#endif
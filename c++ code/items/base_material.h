#ifndef BASE_MATERIAL_H
#define BASE_MATERIAL_H

#include "item.h"					// parent class
#include "crafting_material.h"		// parent class
#include "../utilities/si_units.h"
#include <string>
#include "../utilities/assert.h"

namespace Items {


	class Base_material : public Stackable_item<float>, public Crafting_material {

	public:

		Base_material(Material_type type)
			: Stackable_item(Identifier::crafting_material, type._to_string()),
				Crafting_material(type)
		{
			// if not base material identifier, fail.
			ASSERT(type > Material_type::base_material_MAX, "Base_material must have base material identifier!");
		}

		Base_material(Material_type type, std::string name)
			: Stackable_item(Identifier::crafting_material, name),
				Crafting_material(type)
		{
			// if not base material identifier, fail.
			ASSERT(type > Material_type::base_material_MAX, "Base_material must have base material identifier!");
		}

		float flexibility{0};	// [0,1]
		float toughness{0};		// high toughness -> high armour & high durability

		float get_max_stack_size() const override { return 9999; }
        SI::Base::Mass get_unit_weight() const override { return unit_weight; }

		const std::string get_flags() const override { return Crafting_material::get_flags(); }

	};




	class Decoration_material : public Stackable_item<int>, public Crafting_material {

	public:

		Decoration_material()
			: Stackable_item(Identifier::crafting_material, "decoration"),
				Crafting_material(Material_type::decoration) {}

		Decoration_material(std::string name)
			: Stackable_item(Identifier::crafting_material, name),
				Crafting_material(Material_type::decoration) {}

		int get_max_stack_size() const override { return 9999; }
        SI::Base::Mass get_unit_weight() const override { return unit_weight; }

		const std::string get_flags() const override { return Crafting_material::get_flags(); }
	};


}	// namespace Items

#endif
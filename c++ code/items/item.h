// item.h
#ifndef ITEM_H
#define ITEM_H

#include "../external/string_enum.h"
#include "../utilities/si_units.h"      // mass (class member)
#include <iostream>
#include <string>

namespace Items
{

	// combination of type and material + some other kinds of items
    ENUM(Identifier, int,
        // slots: head, chest, hands, feet, legs, belt, small_shield, large_shield
        // materials: leather, plate, chainmail, cloth, wood

        // leather
        leather_helmet,
        leather_chest,
        leather_full_body,
        leather_gloves,
        leather_boots,
        leather_legs,
        leather_belt,
        leather_small_shield,
        leather_large_shield,

        // metal
        plate_helmet,
        plate_chest,
        plate_full_body,
        plate_gloves,
        plate_boots,
        plate_legs,
        plate_belt,
        plate_small_shield,
        plate_large_shield,

        chainmail_helmet,
        chainmail_chest,
        chainmail_full_body,
        chainmail_gloves,
        chainmail_boots,
        chainmail_legs,
        chainmail_belt,
        chainmail_small_shield,
        chainmail_large_shield,

        // cloth
        cloth_helmet,
        cloth_chest,
        cloth_full_body,
        cloth_gloves,
        cloth_boots,
        cloth_legs,
        cloth_belt,
        cloth_small_shield,
        cloth_large_shield,

        // wood
        wooden_helmet,
        wooden_chest,
        wooden_full_body,
        wooden_gloves,
        wooden_boots,
        wooden_legs,
        wooden_belt,
        wooden_small_shield,
        wooden_large_shield,

        // jewellery
        amulet, ring,

        // weapons:
        dagger,
        sword_1h, sword_2h,
        battle_axe_1h, battle_axe_2h,
        assault_spear_1h, assault_spear_2h,
        polearm,
        warhammer_1h, warhammer_2h,
        fist_weapon,
        bow,
        staff,

        // others
        crafting_material,
        gold,
        unknown
    );

	// the type of item
    ENUM(Equipment_type, int,
        head, chest, full_body, hands, feet, legs, belt,
        small_shield, large_shield, one_handed_weapon, two_handed_weapon,
        amulet, ring,
        none
    );

    // the primary base material
    ENUM(Equipment_material, int,
        plate, chainmail, leather, cloth, wood, jewellery,
        other
    );

    ENUM(Weapon_type, int,
        dagger,
        sword_1h, sword_2h,
        battle_axe_1h, battle_axe_2h,
        assault_spear_1h, assault_spear_2h,
        polearm,
        warhammer_1h, warhammer_2h,
        fist_weapon,
        bow,
        staff,
        none
    );


    Equipment_type get_equipment_type(Identifier i);
    Equipment_material get_equipment_material(Identifier i);
    Weapon_type get_weapon_type(Identifier i);
    bool is_weapon(Identifier i);
    bool is_equipment(Identifier i);
    bool is_ranged(Weapon_type w);
    bool is_two_handed(Weapon_type w);


    namespace {
        static int id = 0;
        int get_unique_id() { return id++; }
        using namespace SI::Base; // for numeric literal operators
    }

    class Item {
    public:
        Item() : unique_identifier{-1} {}
        Item(Identifier i) : identifier{i}, name{i._to_string()} {}
        Item(Identifier i, std::string name) : identifier{i}, name{name} {}
        Item(const Item& item) : name{item.name}, identifier{item.identifier}, unique_identifier{item.unique_identifier} {}

        bool virtual operator==(const Item& that) const { return identifier == that.identifier && name == that.name && unique_identifier == that.unique_identifier; }
        bool virtual operator!=(const Item& that) const { return !(*this == that); }

        virtual const std::string get_flags() const { return ""; }
        virtual const std::string get_meta_data() const { return (get_flags() == "") ? "" : "("+get_flags()+")"; }
        virtual SI::Base::Mass get_weight() const { return unit_weight; };  // total weight

        std::string name{};
        SI::Base::Mass unit_weight{0.1_kg};
        Identifier identifier{Identifier::unknown};
        int unique_identifier{get_unique_id()};
    };


    template<typename Number = int>
    class Stackable_item : public Item {
    public:
        Stackable_item() : Item{} {}
        Stackable_item(Identifier i) : Item{i} {}
        Stackable_item(Identifier i, std::string name) : Item{i, name} {}
        Stackable_item(const Stackable_item& item) : Item{item} {}

        virtual Number get_max_stack_size() const { return 1; }
    };


    std::ostream& operator<<(std::ostream& os, const Identifier& id);
    std::ostream& operator<<(std::ostream& os, const Equipment_type& id);
    std::ostream& operator<<(std::ostream& os, const Equipment_material& id);
    std::ostream& operator<<(std::ostream& os, const Weapon_type& id);
    std::ostream& operator<<(std::ostream& os, const Item& i);

} // namespace Items


#endif












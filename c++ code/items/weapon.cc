#include "weapon.h"
#include "../utilities/assert.h"


namespace Items {

	Weapon::Weapon(Identifier id)
		: Equipment(id), weapon_type{get_weapon_type(id)}
	{
		// if not equipment identifier, fail.
		ASSERT(is_weapon(id), "Weapon must have weapon identifier!");
	}

	std::ostream& operator<<(std::ostream& os, const Weapon& w) {

		os << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

		// Level 20 leather helmet with name "leather helmet"
		os << "Level " << w.level << " " << w.weapon_type
			<< " with name \"" << w.name << "\"\n";

		// Req: level 20, 10 str, 10 dex, 0 int
		os << "Req: level " << w.stat_requirements[level] << ", "
			<< w.stat_requirements[strength] << " str, "
			<< w.stat_requirements[dexterity] << " dex, "
			<< w.stat_requirements[intelligence] << " int\n";

		// Weight: 0.5, Damage: 5.0, Attack speed: 1.00, (5.00 DPS)
		os << std::fixed << std::setprecision(1);
		os << "Weight: " << w.get_weight() << ", Damage: " << w.damage;
		os << std::setprecision(2);
		os << ", Attack Speed: " << w.attack_speed
			<< ", (" << w.damage * w.attack_speed << " DPS)\n";

		os << w.secondary_stats;

		os << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

		return os;
	}

}
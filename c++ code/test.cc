#include <vector>
#include <string>
#include <iostream>
// #include <iomanip>
// #include "items/include_all.h"
// #include "crafting/crafting_table.h"
// #include "crafting/recipe.h"
// #include "utilities/assert.h"
// #include "utilities/random.h"



// using namespace Items;
// using namespace Crafting;

// void reftest() {
// 	int a = 1, b = 2;
// 	int& ref = a;
// 	std::cout << ref << std::endl;
// 	a++;
// 	std::cout << ref << std::endl;
// 	a=8;
// 	std::cout << ref << std::endl;
// 	ref=b;
// 	std::cout << ref << std::endl;
// 	b=10;
// 	std::cout << ref << std::endl;
// }

static int uid = 0;

struct Noisy {
	Noisy(int i) : i{i}, id{uid++} {}
	int i;
	int id;
	~Noisy() {std::cout << "destr " << i << ", " << id << ", " << this << std::endl;}
};

std::ostream& operator<<(std::ostream& os, const Noisy& a)
{
	return os << a.i;
}

void add(std::vector<Noisy>& v) {
	Noisy a{75};
	v.push_back(Noisy(2));
	v.push_back(Noisy(3));
	v.push_back(Noisy(4));
	v.push_back(Noisy(5));
	v.push_back(a);
}

void vtest()
{
    std::vector<Noisy> v{};
    add(v);
    for (auto& a : v)
        std::cout << a << std::endl;
}



struct A {
    A(int i) : i{i} {}
    int i;
};


struct B : public virtual A {
    B(int i) : A{i} {}
};

struct C : public virtual A, public B {
    C(int i) : A{i}, B{i} {}
};



// int main() {
//     B b{4};
//     C c{5};
//     std::cout << b.i << c.i << std::endl;
// 	return 0;
// }











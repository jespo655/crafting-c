#include "recipe.h"
#include "../items/base_material.h"
#include "../items/secondary_stats.h"
#include "crafting_exceptions.h"
#include <string>
#include <sstream>

using namespace Crafting;
using namespace Items;



/*
	// recipe
	Identifier crafted_item_identifier{Identifier::unknown};
	Material_type material_type{Material_type::other};

	std::array<float, nr_base_materials> required_base_material_quantity{};
	std::vector<Identifier> other_required_items{};		// shouldn't contain more than one of each identifier
	float other_req_item_stat_coeff{0.2f};

    float toughness_coefficient{1};     // [0, 1] this notes how much of the material actually give protection
    float min_flexibility{0};
    float max_flexibility{1};        // [0, 1] this notes how flexible the design is, regardless of material
    float flex_coefficient{1};

    // secondary stat coefficients
    Secondary_stats stat_coefficients{1};	// all coefficients are 1 by default
*/









/*
    // base_material

	Secondary_stats secondary_stats{};
	Material_type material_type{Material_type::other};
	int level{0};
	SI::Base::Mass unit_weight{0.1_kg};

	float flexibility{0};	// [0,1]
	float toughness{0};		// high toughness -> high armour & high durability
*/


/*
ENUM(Material_type, int,
    metal, leather, cloth, wood, decoration, other,
    material_MAX = other,
    base_material_MAX = wood
);
*/


	// unit weights:
	//

namespace Crafting {

	float get_level_coefficient(int level)
	{
		// TODO: balance level coefficient
		return pow(2,level/10);
	}

	float get_stat_weights(Base_material material)
	{
		// float m = material.weight.val;
		// float t = material.toughness;
		// float f = material.flexibility;
		float level_coeff = get_level_coefficient(material.level);
		return 1/material.unit_weight.val + 10*material.toughness/level_coeff + 10-2/material.flexibility;
	}

	static Base_material leather_template = Base_material(Material_type::leather, "leather_template");
	static Base_material metal_template = Base_material(Material_type::metal, "metal_template");
	static Base_material cloth_template = Base_material(Material_type::cloth, "cloth_template");
	static Base_material wood_template = Base_material(Material_type::wood, "wood_template");
	static bool inited = false;

	void init_templates() {
		// leather: light and flexible
		leather_template.unit_weight = 0.25_kg;
		leather_template.flexibility = 0.7;
		leather_template.toughness = 0.45;

		// metal: heavy and strong
		metal_template.unit_weight = 0.5_kg;
		metal_template.flexibility = 0.3;
		metal_template.toughness = 1.0;

		// cloth: very light and flexible, but almost no toughness
		cloth_template.unit_weight = 0.15_kg;
		cloth_template.flexibility = 0.9;
		cloth_template.toughness = 0.1;

		// wood: heavy and not very strong or flexible
		wood_template.unit_weight = 0.4_kg;
		wood_template.flexibility = 0.5;
		wood_template.toughness = 0.7;

		inited = true;
	}

	Base_material get_material_template(Material_type type)
	{
		if (!inited) init_templates();
		switch(type) {
			case Material_type::leather: return leather_template;
			case Material_type::metal: return metal_template;
			case Material_type::cloth: return cloth_template;
			case Material_type::wood: return wood_template;
		}
		std::stringstream ss;
		ss << "Material template " << type._to_string() << " doesn't exist";
		std::string what = ss.str();
		throw No_such_recipe{what};
	}

	Base_material get_material_template(Material_type type, int level)
	{
		Base_material bm = get_material_template(type);
		float stat_coefficient = get_level_coefficient(level);
		bm.toughness *= stat_coefficient;
		bm.secondary_stats *= stat_coefficient;
		bm.level = level;
		return bm;
	}

	Base_material get_random_material(Material_type type, int level)
	{
		Base_material bm = get_material_template(type, level);
		bm.secondary_stats = random_stats_with_total(get_level_coefficient(level));
		return bm;
	}

} // namespace Crafting


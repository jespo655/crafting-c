


#ifndef CRAFTING_EXCEPTIONS_H
#define CRAFTING_EXCEPTIONS_H

#include <stdexcept>
#include <string>

namespace Crafting {

	// runtime_error that is thrown if craft is unsuccessful
	struct Cannot_craft_exception : public std::runtime_error {
		explicit Cannot_craft_exception(std::string& what) : runtime_error(what) {}
		explicit Cannot_craft_exception(const char* what) : runtime_error(what) {}
	};

	// trying to use a recipe that doesn't exist
	struct No_such_recipe : public std::runtime_error {
		explicit No_such_recipe(std::string& what) : runtime_error(what) {}
		explicit No_such_recipe(const char* what) : runtime_error(what) {}
	};


} // namespace Crafting

#endif
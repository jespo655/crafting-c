// crafting_table.h
// namespace with static functions for crafting items
// TODO: Implement a way to deterime the level of crafted items!

#ifndef CRAFTING_TABLE_H
#define CRAFTING_TABLE_H

#include "../items/item.h"	// enums (and Item)
#include <vector>		 // std::vector

namespace Items {
	class Equipment;
	class Weapon;
	class Crafting_material;
	template<typename N> class Item_stack;
}

namespace Crafting {

	using namespace Items;

	class Recipe;


	// crafting material pointers are needed for polymorphism
	Equipment craft_equipment(const Recipe& recipe,
		std::vector<Crafting_material*>& materials,
		int crafting_skill,
		bool use_materials = true);

	Weapon craft_weapon(const Recipe& recipe,
		std::vector<Crafting_material*>& materials,
		int crafting_skill,
		bool use_materials = true);

	Item_stack<float> craft_base_material(const Recipe& recipe,
		std::vector<Crafting_material*>& materials,
		int crafting_skill,
		bool use_materials = true);

	void test_potential();
}


#endif





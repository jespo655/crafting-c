#ifndef TEMPLATES_H
#define TEMPLATES_H


namespace Items {
	class Base_material;
    class Material_type;
}

namespace Crafting {

	using Items::Base_material;
	using Items::Material_type;

    float get_level_coefficient(int level);
    float get_stat_weights(Base_material material);
    Base_material get_material_template(Material_type type);
    Base_material get_material_template(Material_type type, int level);
    Base_material get_random_material(Material_type type, int level);

} // namespace Crafting


#endif
#ifndef RECIPE_H
#define RECIPE_H

#include "../items/item.h"	// enums
#include <cmath>	// pow
#include <algorithm> // max
#include "../utilities/si_units.h"
#include "../items/crafting_material.h"	// enum Material_type
#include "../items/secondary_stats.h"	// Secondary_stats
#include <vector>



// Todo: crafting templates


namespace Crafting {

	using Damage = SI::Value<SI::Base::Dimensionless>;
	using DPS = SI::Value<SI::Unit<0,0,-1>>;
	using SI::Base::Mass;
	using SI::Base::Frequency;

	using namespace Items;	// enums Identifier, Weapon_type


	static const int nr_base_materials = Material_type::base_material_MAX+1;


	struct Recipe {

		// crafted item info
		Identifier crafted_item_identifier{Identifier::unknown};
		Material_type material_type{Material_type::other};

		// material requirements
		std::array<float, nr_base_materials> required_base_material_quantity{};
		std::vector<Identifier> other_required_items{};		// shouldn't contain more than one of each identifier
		float other_req_item_stat_coeff{0.2f};
		int max_decorations{0};

		// stat coefficients
		// (weight is always the sum of the weight of the materials used)
        float toughness_coefficient{1};     // [0, 1] this notes how much of the material actually give protection
        float min_flexibility{0};
        float max_flexibility{1};        // [0, 1] this notes how flexible the design is, regardless of material
        float flex_coefficient{1};

        // 0 flexibility on materials -> 0.2 flexibility on item (minFlexibility)
        // 1 flexibility on materials -> 0.8 flexibility on item (maxFlexibility)
        // (the average of all materials * flexCoefficient) give final flexibility

        // secondary stat coefficients
        Secondary_stats stat_coefficients{1};	// all coefficients are 1 by default


	    // on weapons:
	    // armour, weight -> damage (different formula for different weapons)
	    // weight -> attack speed (heavy -> slow)


	    typedef Damage weapon_damage_calculator(Weapon_type, Mass weight, float flexibility, float toughness);
	    typedef Frequency weapon_speed_calculator(Weapon_type, Mass weight, float flexibility, float toughness);

	    // calculate the damage per attack with the weapon
	    // this should be in the range 0-1000 (damage numbers are still being decided on)
        weapon_damage_calculator& weapon_damage_calc = wdc_default;

		// calculate the number of attacks / second with the weapon
	    // this should be in the range 0.5 - 2
        weapon_speed_calculator& weapon_speed_calc = wsc_default;


    private:

		static Damage wdc_default(Weapon_type type, Mass w, float f, float t) {
			if (is_ranged(type))
				return Damage(std::sqrt(w.val * t));	// placeholder for better ranged weapon formula
			if (is_two_handed(type))
				return Damage(std::sqrt(w.val * t) * 1.2f);
			else
				return Damage(std::sqrt(w.val * t));
		}

		static Frequency wsc_default(Weapon_type type, Mass w, float f, float t) {
			if (is_ranged(type))
				return Frequency(0.5f + 1.5f * std::pow(2, -w.val/(5*std::max(0.01f,f))));
			else
				return Frequency(0.5f + 1.5f * std::pow(2, -w.val/4));	// same for 1h and 2h
		}

	};



}

#endif
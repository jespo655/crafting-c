#include "recipe.h"
#include "crafting_table.h"
#include "crafting_exceptions.h"
#include "../items/weapon.h"		// Weapon
#include "../items/equipment.h"		// Equipment
#include "../utilities/random.h"	// Random
#include "../items/item.h"
#include "../items/item_stack.h"
#include "../items/base_material.h"		// Base_material

#include <vector>					// std::vector
#include <cmath>					// std::ceil

#include <iostream>	// NYI messamges
#include <string>	// exception
#include "../utilities/assert.h" // debug

namespace Crafting {

	using namespace Items;

	static const int nr_types = Material_type::material_MAX+1;
	// static const int nr_base_materials = Material_type::base_material_MAX+1;


	// This class holds references to items that could be changed,
	// and should therefore only be used temporarily and then be discarded.


	// diff 	min 	max
	// -100		0		0
	// -20 		0		0.2
	// -10		0		0.6
	// 0		0.8		1
	// 100		1		1

	float min_potential_usage(int level_difference) {
		if (level_difference < -10)
			return 0;
		if (level_difference < 0)
			return 0.8f + level_difference/10.f * 0.8f;	// at level -10, min = 0
		if (level_difference < 100)
			return 0.8f + level_difference/100.f * 0.2f;
		return 1;
	}

	float max_potential_usage(int level_difference) {
		if (level_difference < -100)
			return 0;
		if (level_difference < -20)
			return 0.25f + level_difference/100.f * 0.25f;
		if (level_difference < 0)
			return 1 + level_difference/20.f * 0.8f;
		return 1;
	}


	void test_potential()
	{
		for (int diff = -35; diff <= 0; diff += 1) {
			std::cout << "leveldiff " << diff << ": [" <<
				min_potential_usage(diff) << "-" <<
				max_potential_usage(diff) << "]\n";
		}
	}




	class Available_materials {

	public:

		Available_materials()
			: material_lists_{std::array<std::vector<Crafting_material*>, nr_types>()},
				material_quantity_{std::array<float, nr_types>()}
				{}

		Available_materials(std::vector<Crafting_material*> materials)
			: material_lists_{std::array<std::vector<Crafting_material*>, nr_types>()},
				material_quantity_{std::array<float, nr_types>()}
				{ add_materials(materials); }

		void add_materials(std::vector<Crafting_material*>& materials) {
			for (Crafting_material* mat : materials) {
				if (!mat->is_used()) {
					Material_type type = mat->material_type;
					material_quantity_[type] += mat->get_float_quantity();
					material_lists_[type].push_back(mat);
				}
			}
		}


        // TODO: known bug: can incorrectly find the same item twice
        //      this method will return the wrong result if required materials contains two of the same item and only one is available
        // In the meantime: assume
		bool can_craft_item(const Recipe& recipe) {
			for (int i = 0; i < nr_base_materials; i++)
				if (material_quantity_[i] < recipe.required_base_material_quantity[i])
					return false;

			// No available items are used. Use that variable for the
			// 		temporary check, then switch it back.
			for (Identifier id : recipe.other_required_items) {
				bool item_available = false;
				for (Crafting_material* cmp : material_lists_[Material_type::other]) {
					if (Item* item = dynamic_cast<Item*>(cmp)) {
						if (item->identifier == id) {
							item_available = true;
							break;
						}
					}
				}
				if (!item_available)
					return false;
			}
			return true;
		}


		// // version that would manage two of the same item
		// // 	however, it is not safe for all derivations of Crafting_material that overrides is_used() (for example Item_stack)
		// bool can_craft_item(Recipe& recipe) {
		// 	for (int i = 0; i < nr_base_materials; i++)
		// 		if (material_quantity_[i] < recipe.required_base_material_quantity[i])
		// 			return false;

		// 	// No available items are used. Use that variable for the
		// 	// 		temporary check, then switch it back.
		// 	for (Identifier id : recipe.other_required_items) {
		// 		bool item_available = false;
		// 		for (Crafting_material* cmp : material_lists_[Material_type::other]) {
		// 			if (!cmp->is_used()) {
		// 				if (Item* item = dynamic_cast<Item*>(cmp)) {
		// 					if (item->identifier == id) {
		// 						item_available = true;
		// 						cmp->set_used(true);
		// 						break;
		// 					}
		// 				}
		// 			}
		// 		}
		// 		if (!item_available) {
		// 			// switch back and return false
		// 			for (Crafting_material* cmp : material_lists_[Material_type::other])
		// 				cmp->set_used(false);
		// 			return false;
		// 		}
		// 	}
		// 	// switch back and return true
		// 	for (Crafting_material* cmp : material_lists_[Material_type::other])
		// 		cmp->set_used(false);
		// 	return true;
		// }



		void create_min_max(const Recipe& recipe, bool use_materials, int crafting_skill, Crafting_material* min_stat_eq, Crafting_material* max_stat_eq)
		{
			// base materials
			// use the first available in the list
			for (int i = 0; i < nr_base_materials; i++) {
				float total_mats = recipe.required_base_material_quantity[i];
				float mats_to_use = recipe.required_base_material_quantity[i];
				for (Crafting_material* cmp : material_lists_[i]) {

					mats_to_use = use_material(cmp, min_stat_eq, max_stat_eq, use_materials, crafting_skill,
							mats_to_use, total_mats);

					if (mats_to_use <= 0)
						break;
				}
				ASSERT(mats_to_use <= 0, "Crafting failed in the middle of using materials");
			}

			// decoration materials
			int decorations_to_use = recipe.max_decorations;
			for (Crafting_material* cmp : material_lists_[Material_type::decoration]) {
				float available_decs = cmp->get_float_quantity();
				if (available_decs <= 0 || cmp->is_used())
					continue;

				decorations_to_use = use_decoration(cmp, min_stat_eq, max_stat_eq, use_materials, crafting_skill,
						decorations_to_use);

				if (decorations_to_use <= 0)
					break;
			}

			// other required materials
			// These add weight but very limited stats otherwise.
			for (Identifier id : recipe.other_required_items) {
				for (Crafting_material* cmp : material_lists_[Material_type::decoration]) {
					if (Item* item = dynamic_cast<Item*>(cmp)) {
						if (item->identifier == id) {
							add_stats(cmp, min_stat_eq, max_stat_eq, crafting_skill - cmp->level,
								1, 1, recipe.other_req_item_stat_coeff, false);
							if (use_materials)
								cmp->use();
						}
					}
				}
			}
		}



	private:

		// a list of lists of materials of different types
		std::array<std::vector<Crafting_material*>, nr_types> material_lists_;
		std::array<float, nr_types> material_quantity_;

		// various helper functions
		float use_material(Crafting_material* cmp, Crafting_material* min_stats, Crafting_material* max_stats, bool use_materials, int crafting_skill,
			 float& mats_to_use, float total_mats = 1)
		{
			int level_difference = crafting_skill - cmp->level;
			float available = cmp->get_float_quantity();
			if (available <= 0 || cmp->is_used())
				return mats_to_use;

			if (available <= mats_to_use) {
				// use all of the material
				add_stats(cmp, min_stats, max_stats, level_difference, available, total_mats);
				if (use_materials)
					cmp->use();
				mats_to_use -= available;
			}
			else if (Item_stack<float>* is = dynamic_cast<Item_stack<float>*>(cmp)) {
				// use only some of the material
				add_stats(cmp, min_stats, max_stats, level_difference, mats_to_use, total_mats);
				if (use_materials)
					is->use(mats_to_use);
				mats_to_use = 0;
			}
			else if (Item_stack<int>* is = dynamic_cast<Item_stack<int>*>(cmp)) {
				// Every kind of Item_stack has to be checked individually.
				// use only some of the material
				add_stats(cmp, min_stats, max_stats, level_difference, mats_to_use, total_mats);
				if (use_materials)
					is->use(std::ceil(mats_to_use));
				mats_to_use = 0;
			}
			else {
				// the material is more than enough but can't be split into smaller parts, so use all of it.
				// more material than needed will be used, but not more than 1 more.
				// the extra material will not give any bonus stats.
				add_stats(cmp, min_stats, max_stats, level_difference, mats_to_use, total_mats);
				if (use_materials)
					cmp->use();
				mats_to_use = 0;
			}
			return mats_to_use;
		}


		int use_decoration(Crafting_material* cmp, Crafting_material* min_stats, Crafting_material* max_stats, bool use_materials, int crafting_skill,
				int& mats_to_use)
		{
			int level_difference = crafting_skill - cmp->level;
			int available = static_cast<int>(cmp->get_float_quantity());
			if (available <= 0 || cmp->is_used())
				return mats_to_use;

			if (available <= mats_to_use) {
				// use all of the material
				add_stats(cmp, min_stats, max_stats, level_difference, available);
				if (use_materials)
					cmp->use();
				mats_to_use -= available;
			}
			else if (Item_stack<float>* is = dynamic_cast<Item_stack<float>*>(cmp)) {
				// use only some of the material
				add_stats(cmp, min_stats, max_stats, level_difference, mats_to_use);
				if (use_materials)
					is->use(mats_to_use);
				mats_to_use = 0;
			}
			else if (Item_stack<int>* is = dynamic_cast<Item_stack<int>*>(cmp)) {
				// Every kind of Item_stack has to be checked individually.
				// use only some of the material
				add_stats(cmp, min_stats, max_stats, level_difference, mats_to_use);
				if (use_materials)
					is->use(std::ceil(mats_to_use));
				mats_to_use = 0;
			}
			else {
				// the material is more than enough but can't be split into smaller parts, so use all of it.
				// more material than needed will be used, but not more than 1 more.
				// the extra material will not give any bonus stats.
				add_stats(cmp, min_stats, max_stats, level_difference, mats_to_use);
				if (use_materials)
					cmp->use();
				mats_to_use = 0;
			}
			return mats_to_use;
		}


		void add_stats(Crafting_material* mat, Crafting_material* min, Crafting_material* max, int level_difference,
			float quantity = 1, float total_quantity = 1, float stat_coefficient = 1, bool check_base_mat = true)
		{
			min->get_unit_weight() += mat->get_unit_weight() * quantity;
			max->get_unit_weight() += mat->get_unit_weight() * quantity;

			float min_coeff = min_potential_usage(level_difference) * quantity * stat_coefficient;
			float max_coeff = max_potential_usage(level_difference) * quantity * stat_coefficient;

			min->secondary_stats += mat->secondary_stats * min_coeff;
			max->secondary_stats += mat->secondary_stats * max_coeff;

			if (check_base_mat) {
				if (Base_material* base_mat = dynamic_cast<Base_material*>(mat)) {
					float flexibility = base_mat->flexibility * quantity / total_quantity;
					float toughness = base_mat->toughness;

					if (Equipment* min_eq = dynamic_cast<Equipment*>(min)) {
						if (Equipment* max_eq = dynamic_cast<Equipment*>(max)) {
							min_eq->flexibility += flexibility;
							max_eq->flexibility += flexibility;

							min_eq->armour += toughness * min_coeff;
							max_eq->armour += toughness * max_coeff;
						}
					}

					if (Base_material* min_bm = dynamic_cast<Base_material*>(min)) {
						if (Base_material* max_bm = dynamic_cast<Base_material*>(max)) {
							min_bm->flexibility += flexibility;
							max_bm->flexibility += flexibility;

							min_bm->toughness += toughness * min_coeff;
							max_bm->toughness += toughness * max_coeff;
						}
					}
				}
			}
		}

	};








	// destroys min_stat_eq to make all its stats random between min and max
	// pointers needed for polymorphism
	Equipment* random_between_eq(Equipment* min, const Equipment* max)
	{
        // each stat is a random value between min and max
        // eq.x = coefficient * (min.x + (max.x - min.x) * random_float)
		min->unit_weight += (max->unit_weight - min->unit_weight) * Random::next_float();
		min->armour += (max->armour - min->armour) * Random::next_float();
		min->flexibility += (max->flexibility - min->flexibility) * Random::next_float();

		min->secondary_stats = random_between(min->secondary_stats, max->secondary_stats);
		return min;
	}



	void finalize_eq(Equipment& eq, const Recipe& recipe)
	{
		// finalizes the equipment according to the recipe
		eq.armour *= recipe.toughness_coefficient;
		eq.secondary_stats *= recipe.stat_coefficients;
		// transpose the item's flexibility to between recipe.min and recipe.max
		float flexibility = std::min(1.f, recipe.flex_coefficient * eq.flexibility);
		eq.flexibility = recipe.min_flexibility + (recipe.max_flexibility - recipe.min_flexibility) * flexibility;
	}


	void finalize_wep(Weapon& w, const Recipe recipe)
	{
		// finalizes the weapon according to the recipe
		w.armour *= recipe.toughness_coefficient;
		w.secondary_stats *= recipe.stat_coefficients;
		// transpose the item's flexibility to between recipe.min and recipe.max
		float flexibility = std::min(1.f, recipe.flex_coefficient * w.flexibility);
		w.flexibility = recipe.min_flexibility + (recipe.max_flexibility - recipe.min_flexibility) * flexibility;

		w.damage = recipe.weapon_damage_calc(w.weapon_type, w.get_weight(), w.flexibility, w.armour);
		w.attack_speed = recipe.weapon_speed_calc(w.weapon_type, w.get_weight(), w.flexibility, w.armour);
	}






	// crafting material pointers is needed for polymorphism
	Weapon craft_weapon(const Recipe& recipe, std::vector<Crafting_material*>& materials, int crafting_skill, bool use_materials)
	{
		if (!is_weapon(recipe.crafted_item_identifier)) throw Cannot_craft_exception("Not weapon identifier!");

		Available_materials am{materials};
		if (!am.can_craft_item(recipe)) throw Cannot_craft_exception("Not enough materials!");

		Weapon max_stat_wep{recipe.crafted_item_identifier};
		Weapon min_stat_wep{recipe.crafted_item_identifier};

		am.create_min_max(recipe, use_materials, crafting_skill, &min_stat_wep, &max_stat_wep);
		Weapon w = *dynamic_cast<Weapon*>(random_between_eq(static_cast<Equipment*>(&min_stat_wep), static_cast<Equipment*>(&max_stat_wep)));

		finalize_wep(w, recipe);

		// TODO: Determine level, name, stat_requirements
		w.level = crafting_skill; 	// temporary
		return w;
	}

	Equipment craft_equipment(const Recipe& recipe, std::vector<Crafting_material*>& materials, int crafting_skill, bool use_materials)
	{
		if (!is_equipment(recipe.crafted_item_identifier)) throw Cannot_craft_exception("Not equipment identifier!");

		Available_materials am{materials};
		if (!am.can_craft_item(recipe)) throw Cannot_craft_exception("Not enough materials!");

		Equipment max_stat_eq{recipe.crafted_item_identifier};
		Equipment min_stat_eq{recipe.crafted_item_identifier};

		am.create_min_max(recipe, use_materials, crafting_skill, &min_stat_eq, &max_stat_eq);
		Equipment eq = *(random_between_eq(&min_stat_eq, &max_stat_eq));

		finalize_eq(eq, recipe);

		// TODO: Determine level, name, stat_requirements
		eq.level = crafting_skill; 	// temporary
		return eq;
	}


	Item_stack<float> craft_base_material(const Recipe& recipe, std::vector<Crafting_material*>& materials, int crafting_skill, bool use_materials)
	{
		if (recipe.crafted_item_identifier != Identifier::crafting_material || recipe.material_type > Material_type::base_material_MAX)
			throw Cannot_craft_exception("Not base material identifier!");

		Available_materials am{materials};
		if (!am.can_craft_item(recipe)) throw Cannot_craft_exception("Not enough materials!");

		Base_material min_stat_bm{recipe.material_type};
		Base_material max_stat_bm{recipe.material_type};

		am.create_min_max(recipe, use_materials, crafting_skill, &min_stat_bm, &max_stat_bm);
		Base_material& bm = min_stat_bm;	// rename for easier readability

		bm.secondary_stats = random_between(min_stat_bm.secondary_stats, max_stat_bm.secondary_stats);

		// TODO: Determine level, name, stat_requirements
		bm.level = crafting_skill; 	// temporary

		return Item_stack<float>(&bm, 1);
	}


}